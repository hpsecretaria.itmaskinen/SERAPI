﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class UserModuleRoleRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public UserModuleRoleRepository()
        {
            db = new SERAppDBContext(connString);
        }
        #region ROLES
        public List<Role> GetAllUserRoles()
        {
            return db.Roles.Where(r => r.IsUser).ToList();
        }

        public List<Role> GetAllModuleRoles()
        {
            return db.Roles.Where(r => r.IsModule).ToList();
        }

        public List<Role> GetAllRoles()
        {
            return db.Roles.ToList();
        }

        public Role GetUserRole(int userId) {
            var data = db.UserRoles.Where(u => u.UserId == userId).FirstOrDefault();
            var role = db.Roles.Where(r => r.Id == data.RoleId).SingleOrDefault();
            return role; // assuming there should only be one role per user
        }

        public void SaveUserRole()
        {

        }
        #endregion

        #region USERMODULEROLES

        public List<UserModuleRoles> GetUserModuleRolesById(int userId) {
            return db.UserModuleRoles.Where(u => u.UserId == userId).ToList();
        }

        public List<UserModuleRoles> GetUserModuleRolesBySiteId(int siteId)
        {
            if (siteId == -1)
            {
                return db.UserModuleRoles.Include(r => r.User).Include(r => r.Role).ToList();
            }

            return db.UserModuleRoles.Include(r=>r.User).Include(r=>r.Role).Where(u => u.SiteId == siteId).ToList();
        }

        public List<UserModuleRoles> GetUserModuleRolesBySiteIds(List<int> siteId)
        {
            return db.UserModuleRoles.Include(r => r.User).Include(r => r.Role).Where(u => siteId.Contains(u.SiteId.Value)).ToList();
        }

        public void SaveUserModuleRole(UserModuleRoleModel model) {
            var data = db.UserModuleRoles.Where(u => u.ModuleId == model.ModuleId && u.UserId == model.UserId && u.SiteId == model.SiteId).SingleOrDefault();
            if (data != null || model.RoleId == 0)
            {
                db.UserModuleRoles.Remove(data);
                db.SaveChanges();

                if (model.RoleId > 0) {
                    UserModuleRoles userModuleRole = new UserModuleRoles()
                    {
                        RoleId = model.RoleId,
                        UserId = model.UserId,
                        SiteId = model.SiteId,
                        ModuleId = model.ModuleId,
                        CreatedDate = DateTime.Now
                    };
                    db.UserModuleRoles.Add(userModuleRole);
                    db.SaveChanges();
                }
            }
            else {
                UserModuleRoles userModuleRole = new UserModuleRoles()
                {
                    RoleId = model.RoleId,
                    UserId = model.UserId,
                    SiteId = model.SiteId,
                    ModuleId = model.ModuleId,
                    CreatedDate = DateTime.Now
                };
                db.UserModuleRoles.Add(userModuleRole);
                db.SaveChanges();
            }

        }

        public void DeleteUserRole() {

        }

        public void DeleteUserModuleRoleByUserId(int userId) {
            db.UserModuleRoles.ToList().RemoveAll(u => u.UserId == userId);
            db.SaveChanges();
        }

        public List<UserModuleRoles> GetAllBySite(int SiteId)
        {
            return db.UserModuleRoles.Where(x => x.SiteId == SiteId).ToList();
        }
        #endregion
    }
}
