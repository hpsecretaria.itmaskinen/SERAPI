﻿using SERApp.Data.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        protected SERAppDBContext dbContext;
        public Repository()
        {
            dbContext = new SERAppDBContext(connString);
        }

        public T Save(T data)
        {
            dbContext.Set<T>().Add(data);
            dbContext.SaveChanges();

            return data;
        }
        public IQueryable<T> GetAll()
        {            
            return dbContext.Set<T>();
        }
        public void Delete(int id)
        {
            var data = dbContext.Set<T>().Find(id);
            dbContext.Set<T>().Attach(data);
            dbContext.Set<T>().Remove(data);
            dbContext.SaveChanges();
        }
        public IQueryable<T> GetByPredicate(Func<T, bool> predicate)
        {
            return dbContext.Set<T>().Where(predicate).AsQueryable();
        }
        public T Get(int Id)
        {
            return dbContext.Set<T>().Find(Id);
        }
        public void Update(T data)
        {
            dbContext.Set<T>().AddOrUpdate(data);
            dbContext.SaveChanges();
        }
    }

    //public static class IncludeExtension
    //{
    //    public static IQueryable<TEntity> Include<TEntity>(this IDbSet<TEntity> dbSet,
    //                                            params Expression<Func<TEntity, object>>[] includes)
    //                                            where TEntity : class
    //    {
    //        IQueryable<TEntity> query = null;
    //        foreach (var include in includes)
    //        {
    //            query = dbSet.Include(include);
    //        }

    //        return query == null ? dbSet : query;
    //    }
    //}
}
