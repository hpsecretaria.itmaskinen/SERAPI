﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class IssueWebRepository : Repository<IssueWeb>, IRepository<IssueWeb>, IIssueWebRepository
    {
        public IEnumerable<IssueWeb> GetAllIncludingChildren(int accountId)
        {
            return dbContext.IssueWebs.Include(x=>x.Site)
                .Include(x => x.IssueType1)
                .Include(x => x.IssueType2)
                .Include(x => x.Tenant)      
                .Where(x=>x.AccountId == accountId)
                .ToList();
        }

        public IssueWeb GetInlcudingChildrenId(int id)
        {
            return dbContext.IssueWebs
                 .Include(x => x.Site)
                 .Include(x => x.IssueType1)
                 .Include(x => x.IssueType2)
                 .Include(x => x.Tenant)
                 .Include(x =>x.IssueWebHistories)
                 .SingleOrDefault(r => r.IssueWebId == id);
        }

        public void AddIssueWebHistory(IssueWebHistory data)
        {
            dbContext.IssueWebHistories.Add(data);
            dbContext.SaveChanges();
        }

        public List<IssueWebHistory> GetIssueWebHistoryByIssueWebId(int IssueWebId)
        {
            return dbContext.IssueWebHistories.Where(x => x.IssueWebId == IssueWebId).ToList();
        }
    }
}
