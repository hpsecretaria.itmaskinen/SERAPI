﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class DailyReportFieldsRepository : Repository<DailyReportFields>, IDailyReportFieldsRepository
    {
        public new List<DailyReportFields> GetAll()
        {
            return this.dbContext.DailyReportFields.Include("FieldType").ToList();
        }
    }
}
