﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class DepartmentRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public DepartmentRepository() {
            db = new SERAppDBContext(connString);
        }

        public Department Get(int id) {
            return db.Departments.Where(d => d.Id == id).SingleOrDefault();
        }

        public List<Department> GetAllDepartments()
        {
            return db.Departments.ToList();
        }

        public List<Department> GetAllDepartmentsByAccountId(int accountId) {
            return db.Departments.Where(d => d.AccountId == accountId).ToList();
        }

        public void SaveDepartment(DepartmentModel model) {
            var data = db.Departments.Where(d => d.Id == model.Id).SingleOrDefault();
            if (data == null)
            {
                Department newDepartment = new Department()
                {
                    Name = model.Name,
                    Description = model.Description,
                    DepartmentHead = model.DepartmentHead,
                    ContactNumber = model.ContactNumber,
                    AccountId = model.AccountId,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                };
                db.Departments.Add(newDepartment);
            }
            else {
                data.Name = model.Name;
                data.Description = model.Description;
                data.DepartmentHead = model.DepartmentHead;
                data.ContactNumber = model.ContactNumber;
                data.AccountId = model.AccountId;
                data.CreatedDate = DateTime.Now;
                data.LastModifiedDate = DateTime.Now;
            }
            db.SaveChanges();
        }

        public void Delete(int id) {
            var data = db.Departments.SingleOrDefault(x => x.Id == id);
            db.Departments.Remove(data);
            db.SaveChanges();
        }
    }
}
