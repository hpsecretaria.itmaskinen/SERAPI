﻿using SERApp.Data.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class FacilityRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public FacilityRepository() {
            db = new SERAppDBContext(connString);
        }

        public Facility Get(int id)
        {
            return db.Facilities.Where(f => f.Id == id).SingleOrDefault();
        }

        public List<Facility> GetAll() {
            return db.Facilities.ToList();
        }

    }
}
