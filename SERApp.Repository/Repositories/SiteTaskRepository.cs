﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class SiteTaskRepository : Repository<SiteTask>, IRepository<SiteTask>, ISiteTaskRepository
    {
        public IEnumerable<SiteTask> GetAllSiteTasksChildIncluded()
        {
            return dbContext.SiteTasks.Include(v => v.Site).Include(v => v.Task)                
                .ToList();
        }
    }
}
