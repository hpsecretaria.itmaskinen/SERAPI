USE [SERAppDB]
GO
/****** Object:  Table [dbo].[Type3]    Script Date: 5/26/2018 11:02:18 AM ******/
DROP TABLE [dbo].[Type3]
GO
/****** Object:  Table [dbo].[Type2]    Script Date: 5/26/2018 11:02:18 AM ******/
DROP TABLE [dbo].[Type2]
GO
/****** Object:  Table [dbo].[Type1]    Script Date: 5/26/2018 11:02:18 AM ******/
DROP TABLE [dbo].[Type1]
GO
/****** Object:  Table [dbo].[Summary]    Script Date: 5/26/2018 11:02:18 AM ******/
DROP TABLE [dbo].[Summary]
GO
/****** Object:  Table [dbo].[Incidents]    Script Date: 5/26/2018 11:02:18 AM ******/
DROP TABLE [dbo].[Incidents]
GO
/****** Object:  Table [dbo].[Incidents]    Script Date: 5/26/2018 11:02:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Incidents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NULL,
	[Quantity] [int] NULL,
	[PoliceResponse] [int] NULL,
	[ActionDescription] [nvarchar](250) NULL,
	[Type1Id] [int] NULL,
	[Type2Id] [int] NULL,
	[Type3Id] [int] NULL,
	[TenantId] [int] NULL,
	[AccountId] [int] NULL,
	[SiteId] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Incidents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Summary]    Script Date: 5/26/2018 11:02:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Summary](
	[SummaryId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[SummaryValue] [varchar](max) NULL,
 CONSTRAINT [PK_Summary] PRIMARY KEY CLUSTERED 
(
	[SummaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Type1]    Script Date: 5/26/2018 11:02:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type1](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[IsReportRequired] [bit] NULL,
	[RequiredReport] [nvarchar](250) NULL,
	[ShownReport] [nvarchar](250) NULL,
 CONSTRAINT [PK_IncidentTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Type2]    Script Date: 5/26/2018 11:02:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type2](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[CreatedDate] [date] NULL,
	[LastUpdatedDate] [date] NULL,
 CONSTRAINT [PK_CustomTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Type3]    Script Date: 5/26/2018 11:02:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Type3](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[IsTenantRequired] [bit] NULL,
	[AccountId] [int] NULL,
 CONSTRAINT [PK_Type3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
