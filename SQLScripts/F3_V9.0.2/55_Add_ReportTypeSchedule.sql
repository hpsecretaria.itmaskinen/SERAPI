GO

/***** Object:  Table [dbo].[ReportTypeSchedule]    Script Date: 2/11/2019 1:00:07 PM *****/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReportTypeSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportTypeId] [int] NOT NULL,
	[ModuleId] [int] NOT NULL,
	[AccountId] [int] NOT NULL,
	[Schedule] [varchar](500) NOT NULL,
	[Cron] [varchar](500) NOT NULL,
 CONSTRAINT [PK_ReportTypeSchedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
