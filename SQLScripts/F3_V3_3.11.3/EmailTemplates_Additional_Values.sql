
declare @adminModuleId int,@dailyReportModuleId int;

SELECT @adminModuleId = Id FROM Modules WHERE ShortName = 'administration';
SELECT @dailyReportModuleId = Id FROM Modules WHERE ShortName = 'dailyreport';


INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId) VALUES  
('ForgotPassword','Forgot Password','SER FORGOT/CHANGE PASSWORD','Change your password by clicking this link.  {changePassLink} <br /> Please make sure change your password as often as possible to avoid security issues.',NULL,'False',@adminModuleId);

INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId) VALUES  
('DailyReport','Daily Report','SER Daily Report','We have attached a copy of the daily report. <br/><br/>Thanks.',NULL,'False',@dailyReportModuleId);

INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId) VALUES  
('WeeklyReport','Weekly Report','SER Weekly Report','We have attached a copy of the weekly report. <br/><br/>Thanks.',NULL,'False',@dailyReportModuleId);


declare @forgotPasswordEmailTemplateId int,@dailyReportEmailTemplateId int,@weeklyReportEmailTemplateId int;

SELECT @forgotPasswordEmailTemplateId = Id FROM EmailTemplates WHERE RoutineName ='ForgotPassword';
SELECT @dailyReportEmailTemplateId = Id FROM EmailTemplates WHERE RoutineName ='DailyReport';
SELECT @weeklyReportEmailTemplateId = Id FROM EmailTemplates WHERE RoutineName ='WeeklyReport';

INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@forgotPasswordEmailTemplateId,1,'SER FORGOT/CHANGE PASSWORD','Change your password by clicking this link.  {changePassLink} <br /> Please make sure change your password as often as possible to avoid security issues.','SMS Body (Not Set)');


INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@dailyReportEmailTemplateId,1,'SER4 Daily Report','We have attached a copy of the daily report. Thanks','SMS Body (Not Set)');


INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@weeklyReportEmailTemplateId,1,'SER4  Week Report','We have attached a copy of the Week report. Thanks','SMS Body (Not Set)');


