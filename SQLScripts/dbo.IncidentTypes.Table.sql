USE [SERAppDB]
GO
/****** Object:  Table [dbo].[IncidentTypes]    Script Date: 4/4/2018 8:44:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IncidentTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[IsReportRequired] [bit] NULL,
	[RequiredReport] [nvarchar](250) NULL,
	[ShownReport] [nvarchar](250) NULL,
 CONSTRAINT [PK_IncidentTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
