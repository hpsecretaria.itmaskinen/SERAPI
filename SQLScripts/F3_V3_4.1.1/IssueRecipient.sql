USE [SER4prod]
GO
/****** Object:  Table [dbo].[IssueRecipients]    Script Date: 9/24/2018 2:25:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IssueRecipients](
	[IssueRecipientId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](250) NULL,
	[IssueRecipientTypeId] [int] NULL,
	[SiteId] [int] NULL,
 CONSTRAINT [PK_IssueRecipients] PRIMARY KEY CLUSTERED 
(
	[IssueRecipientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IssueRecipientTypes]    Script Date: 9/24/2018 2:25:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IssueRecipientTypes](
	[IssueRecipientTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_IssueRecipientTypes] PRIMARY KEY CLUSTERED 
(
	[IssueRecipientTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[IssueRecipientTypes] ON 

INSERT [dbo].[IssueRecipientTypes] ([IssueRecipientTypeId], [Name]) VALUES (1, N'Technician Email')
INSERT [dbo].[IssueRecipientTypes] ([IssueRecipientTypeId], [Name]) VALUES (2, N'Reminder Email')
INSERT [dbo].[IssueRecipientTypes] ([IssueRecipientTypeId], [Name]) VALUES (3, N'Extra Recipient')
INSERT [dbo].[IssueRecipientTypes] ([IssueRecipientTypeId], [Name]) VALUES (4, N'Emergency Email')
INSERT [dbo].[IssueRecipientTypes] ([IssueRecipientTypeId], [Name]) VALUES (5, N'Facility Email')
SET IDENTITY_INSERT [dbo].[IssueRecipientTypes] OFF
