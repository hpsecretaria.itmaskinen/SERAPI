
declare @IssueModuleId int;
SELECT @IssueModuleId = Id FROM Modules WHERE ShortName = 'issues';

INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId)
VALUES ('TechnicianEmail','Technician Email','Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email. <br/><br/><a href="{appurl}issues/{IssueWebId}/confirm">Click here to confirm this issue</a><br/><br/>Thanks,<br/>SER4 Application ',
'NotSet','false',@IssueModuleId);

INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId)
VALUES ('ReminderEmail','Reminder Email','Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'NotSet','false',@IssueModuleId);

INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId)
VALUES ('ExtraRecipient','Extra Recipient','Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'NotSet','false',@IssueModuleId);


INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId)
VALUES ('EmergencyEmail','Emergency Email','Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'NotSet','false',@IssueModuleId);


INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId)
VALUES ('FacilityEmail','Facility Email','Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'NotSet','false',@IssueModuleId);

declare @TechnicianEmail int,@ReminderEmail int,@ExtraRecipient int,@EmergencyEmail int,@FacilityEmail int;

SELECT @TechnicianEmail = Id FROM EmailTemplates WHERE RoutineName ='TechnicianEmail';
SELECT @ReminderEmail = Id FROM EmailTemplates WHERE RoutineName ='ReminderEmail';
SELECT @ExtraRecipient = Id FROM EmailTemplates WHERE RoutineName ='ExtraRecipient';
SELECT @EmergencyEmail = Id FROM EmailTemplates WHERE RoutineName ='EmergencyEmail';
SELECT @FacilityEmail = Id FROM EmailTemplates WHERE RoutineName ='FacilityEmail';


INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@TechnicianEmail,1,
'Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email. <br/><br/><a href="{appurl}issues/{IssueWebId}/confirm">Click here to confirm this issue</a><br/><br/>Thanks,<br/>SER4 Application ',
'SMS Body (Not Set)');

INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@ReminderEmail,1,
'Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'SMS Body (Not Set)');

INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@ExtraRecipient,1,
'Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'SMS Body (Not Set)');

INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@EmergencyEmail,1,
'Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'SMS Body (Not Set)');

INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@FacilityEmail,1,
'Issue # {IssueWebId}',
'Hi Admin, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email.<br/><br/>Thanks,<br/>SER4 Application',
'SMS Body (Not Set)');

