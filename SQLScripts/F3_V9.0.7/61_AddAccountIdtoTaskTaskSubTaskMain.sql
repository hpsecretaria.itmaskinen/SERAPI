ALTER TABLE Task
ADD AccountId int;

ALTER TABLE MainCategory
ADD AccountId int;

ALTER TABLE SubCategory
ADD AccountId int;