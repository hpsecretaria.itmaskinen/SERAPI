UPDATE DailyReportFields SET FieldTypeId = 5 WHERE Id IN (46, 47)
UPDATE DailyReportFields SET FieldTypeId = 7 WHERE Id IN (40)

DELETE FROM Reports WHERE IncidentId NOT IN (SELECT Id FROM Incidents)
DELETE FROM ReportImage WHERE ReportId NOT IN (SELECT Id FROM Reports)

ALTER TABLE [dbo].[Reports]  WITH CHECK ADD  CONSTRAINT [FK_Reports_Incidents] FOREIGN KEY([IncidentId])
REFERENCES [dbo].[Incidents] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Reports] CHECK CONSTRAINT [FK_Reports_Incidents]
GO

ALTER TABLE [dbo].[ReportImage]  WITH CHECK ADD  CONSTRAINT [FK_ReportImage_Reports] FOREIGN KEY([ReportId])
REFERENCES [dbo].[Reports] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ReportImage] CHECK CONSTRAINT [FK_ReportImage_Reports]
GO