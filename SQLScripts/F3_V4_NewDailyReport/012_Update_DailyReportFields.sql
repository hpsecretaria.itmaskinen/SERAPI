ALTER TABLE DailyReportFields
ADD 
ControlName VARCHAR(250) NULL,
DefaultCustomScript VARCHAR(MAX) NULL
GO

ALTER TABLE Type1_Fields
ADD 
CustomScript VARCHAR(MAX) NULL
GO

ALTER TABLE Field_Values
ADD
[Type] VARCHAR(250) NULL
GO

UPDATE DailyReportFields SET ControlName = 'TYPE2' WHERE Id = 25
GO
UPDATE DailyReportFields SET ControlName = 'PLAN' WHERE Id = 26
GO
UPDATE DailyReportFields SET ControlName = 'DATE' WHERE Id = 27
GO
UPDATE DailyReportFields SET ControlName = 'TIME' WHERE Id = 28
GO
UPDATE DailyReportFields SET ControlName = 'GMTYPE' WHERE Id = 29
GO
UPDATE DailyReportFields SET ControlName = 'NOPE' WHERE Id = 30
GO
UPDATE DailyReportFields SET ControlName = 'PVA' WHERE Id = 31
GO
UPDATE DailyReportFields SET ControlName = 'GUARDS' WHERE Id = 32
GO
UPDATE DailyReportFields SET ControlName = 'TYPE3' WHERE Id = 33
GO
UPDATE DailyReportFields SET ControlName = 'DESCRIPTION' WHERE Id = 35
GO
UPDATE DailyReportFields SET ControlName = 'BLUELIGHTOPTION' WHERE Id = 36
GO
UPDATE DailyReportFields SET ControlName = 'WITHBLUELIGHT' WHERE Id = 37
GO
UPDATE DailyReportFields SET ControlName = 'WITHOUTBLUELIGHT' WHERE Id = 38
GO
UPDATE DailyReportFields SET ControlName = 'ADDITIONALTEXT' WHERE Id = 39
GO
UPDATE DailyReportFields SET ControlName = 'POLICEREPORT' WHERE Id = 40
GO
UPDATE DailyReportFields SET ControlName = 'BY' WHERE Id = 41
GO
UPDATE DailyReportFields SET ControlName = 'GUARD' WHERE Id = 42
GO
UPDATE DailyReportFields SET ControlName = 'ORDERNUMBER' WHERE Id = 43
GO
UPDATE DailyReportFields SET ControlName = 'SECTION' WHERE Id = 44
GO
UPDATE DailyReportFields SET ControlName = 'ADDRESS' WHERE Id = 45
GO
UPDATE DailyReportFields SET ControlName = 'CLOSEFROM' WHERE Id = 46
GO
UPDATE DailyReportFields SET ControlName = 'CLOSETO' WHERE Id = 47
GO
UPDATE DailyReportFields SET ControlName = 'REASON' WHERE Id = 48
GO
UPDATE DailyReportFields SET ControlName = 'IMAGE' WHERE Id = 49
GO

UPDATE DailyReportFields SET DefaultCustomScript = 'form.value["BLUELIGHTOPTION"] === "utan"' WHERE Id = 37
GO
UPDATE DailyReportFields SET DefaultCustomScript = 'form.value["BLUELIGHTOPTION"] === "med"' WHERE Id = 38
GO