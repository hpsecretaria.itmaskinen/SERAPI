USE [SER4]
GO

ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [FK_Type1_Fields_DailyReportFields]
GO

ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [DF_Type1_Fields_SortOrder]
GO

ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [DF_Type1_Fields_IsRequired]
GO

/****** Object:  Table [dbo].[Type1_Fields]    Script Date: 1/9/2019 1:18:48 PM ******/
DROP TABLE [dbo].[Type1_Fields]
GO

/****** Object:  Table [dbo].[Type1_Fields]    Script Date: 1/9/2019 1:18:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Type1_Fields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type1Id] [int] NULL,
	[FieldId] [int] NULL,
	[Label] [varchar](500) NOT NULL,
	[DataSource] [varchar](max) NULL,
	[Value] [varchar](max) NULL,
	[IsRequired] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[ExcludedValues] [varchar](max) NULL,
 CONSTRAINT [PK_Type1_Fields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Type1_Fields] ADD  CONSTRAINT [DF_Type1_Fields_IsRequired]  DEFAULT ((0)) FOR [IsRequired]
GO

ALTER TABLE [dbo].[Type1_Fields] ADD  CONSTRAINT [DF_Type1_Fields_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO

ALTER TABLE [dbo].[Type1_Fields]  WITH CHECK ADD  CONSTRAINT [FK_Type1_Fields_DailyReportFields] FOREIGN KEY([FieldId])
REFERENCES [dbo].[DailyReportFields] ([Id])
GO

ALTER TABLE [dbo].[Type1_Fields] CHECK CONSTRAINT [FK_Type1_Fields_DailyReportFields]
GO


