USE [SER4]
GO

/****** Object:  Table [dbo].[Field_Values]    Script Date: 1/17/2019 1:48:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Field_Values](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IncidentId] [int] NULL,
	[Type1_FieldId] [int] NULL,
	[Value] [varchar](max) NOT NULL,
	[Type] [varchar](250) NOT NULL,
 CONSTRAINT [PK_Field_Values] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


