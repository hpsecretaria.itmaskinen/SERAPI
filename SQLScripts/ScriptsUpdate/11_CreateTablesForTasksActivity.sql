CREATE TABLE MainCategory(Id int, Name varchar(250));
CREATE TABLE SubCategory(Id int, Name varchar(250));
CREATE TABLE Task(Id int, TaskNo varchar(250), Title varchar(250), Purpose varchar(250), Description varchar(250), Interval int, NuberOfDays int);

CREATE TABLE TaskMainCategory(Id int,MainCategoryId int, TaskId int);
CREATE TABLE TaskSubCategory(Id int,SubCategoryId int, TaskId int);
