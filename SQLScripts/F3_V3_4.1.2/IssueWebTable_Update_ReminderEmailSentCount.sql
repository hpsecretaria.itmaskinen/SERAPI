ALTER TABLE IssueWeb
ADD ReminderEmailSentCount int default 0;

UPDATE IssueWeb SET ReminderEmailSentCount = 0;