declare @data int;

SELECT @data=Id FROM EmailTemplates WHERE RoutineName = 'SendURL';

INSERT INTO EmailTemplateVariables VALUES('Link','{url}',@data);