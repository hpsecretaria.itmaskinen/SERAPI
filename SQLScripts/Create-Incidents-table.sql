USE [SERAppDB]
GO
/****** Object:  Table [dbo].[Incidents]    Script Date: 5/6/2018 3:15:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Incidents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NULL,
	[Quantity] [int] NULL,
	[PoliceResponse] [int] NULL,
	[ActionDescription] [nvarchar](250) NULL,
	[IncidentTypeId] [int] NULL,
	[CustomTypeId] [int] NULL,
	[TenantId] [int] NULL,
	[AccountId] [int] NULL,
	[SiteId] [int] NULL,
 CONSTRAINT [PK_Incidents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
