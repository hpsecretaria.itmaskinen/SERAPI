import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class TenantService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getTenantList(page:number, pageSize: number = 10, filter: any): Observable<IPagedResults<any>>{
        let url = `Tenant/GetAll`;    
        return super.getAll<any>(url);
    }

    public getTenantByAccountId(id?: number): Observable<any>{
        let url = `Tenant/GetAllByAccountId?accountId=${id}`;
        return super.getAll<any>(url);
    }

    public getTenantBySiteId(id?: number): Observable<any>{
        let url = `Tenant/GetAllBySiteId?siteId=${id}`;
        return super.getAll<any>(url);
    }

    public getTenant(id?: number): Observable<any>{
         let url = `Tenant/Get?id=${id}`;
        return super.getAll<any>(url);
    }

    public saveTenant(data){
        let url = `Tenant/Save`;
        return super.save<any>(url,data);
    }

    public deleteTenant(data){
         let url = `Tenant/Delete`;
        return super.save<any>(url,data);
    }
}
