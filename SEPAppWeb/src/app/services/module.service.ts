import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class ModuleService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getModulesList(page:number, pageSize: number = 10, filter: any): Observable<IPagedResults<any>>{
        let url = `Module/GetAll`;    
        return super.getAll<any>(url);
    }
}
