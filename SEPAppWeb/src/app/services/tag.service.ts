import { Injectable } from '@angular/core';
import { BaseDataService, DataService } from '../core/services';
import { API_URL } from '../constants';
import { Observable } from 'rxjs';

@Injectable()
export class TagService extends BaseDataService {

  constructor(protected dataService: DataService) 
  {
    super(dataService, API_URL);
  }

  public getAllTags(id?:number): Observable<any>{
    let url = `Tag/GetAll?id=${id}`;    
    return super.getAll<any>(url);
  }

  public getTag(id?: number): Observable<any>{
    let url = `Tag/Get?id=${id}`;
    return super.getAll<any>(url);
  }

  public saveTag(data){
    let url = `Tag/Save`;
    return super.save<any>(url,data);
  }

  public deleteTag(data){
      let url = `Tag/Delete`;
      return super.save<any>(url,data);
  }

}
