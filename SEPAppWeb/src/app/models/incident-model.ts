import { IncidentTypeModel } from "./incident-type-model";
import { CustomTypeModel } from "./custom-type-model";
import { TenantsModel } from ".";
import { IncidentGuardModel } from "./incident-guard-model";
import { ReportModel } from "./report-model";

export class IncidentModel {
    public Id : number; 
    public Time: Date;
    public Quantity: number; 
    public PoliceResponse : string;

    public ActionDescription : string;

    public IncidentTypeId : number;
    public IncidentType : IncidentTypeModel;

    public CustomTypeId : number;
    public CustomType : CustomTypeModel;

    public TenantId: number;
    public Tenant: TenantsModel;

    public IncidentGuard : IncidentGuardModel[];
    public Reports : ReportModel[];
}
