import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

// under systemjs, moment is actually exported as the default export, so we account for that
const momentConstructor: (value?: any) => moment.Moment = (<any>moment).default || moment;

@Pipe({
    name: 'localDateFormat'
})
export class LocalDateFormatPipe implements PipeTransform {
    transform(value: string, ...args: any[]): string {
        if (!value) return '';
        let date = new Date(value);
        return momentConstructor(date).format(args[0]);
    }
}