import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AccountModel } from '../../models';
import { AccountService } from '../../services';

import { AccountsModuleListComponent } from '../management/account-module/accounts-module-list.component';
import { UsersListComponent } from '../users/users-list.component';
import { SitesListComponent } from '../sites/sites-list.component';

import { NgbTab, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-accounts-edit',
    templateUrl: './accounts-edit.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    providers: [NgbTabset]
})
export class AccountsEditComponent implements OnInit {
    @ViewChild('accountsModuleList') accountsModuleList: AccountsModuleListComponent;
    @ViewChild('usersList') usersList: UsersListComponent;
    @ViewChild('sitesList') sitesList: SitesListComponent;
    @ViewChild('accountTabs') ngAccountTab: NgbTabset;
    model : any;
    accountId: number = 0;
    accountsModuleListObject: any;
    mode: string = '';
    isAccountEditMode = false;
    canSave = false;
    isSuperAdmin = false;
    isAdmin = false;
    public showSpinner: boolean = false;
    constructor(
        private accountService: AccountService, 
        private tabSet: NgbTabset,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private modalService: NgbModal) {

        }

    public ngOnInit() : void {
         this.route.params.subscribe((params) => {
            if (!params) {
                return;
            }
            const id = params['id'];
            this.mode = params['mode'];
            this.accountId = id;
            this.model = new AccountModel();
            this.loadData(id);
            if(this.mode)
            {
                setTimeout(() => {
                    this.accountSubModulesMode();
                }, 1000)
            }
            else {
                this.isAccountEditMode = true;
            }
            
        });
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'superadmin')
        {
            this.isSuperAdmin = true;
            this.canSave = true;
        }
        else if(currentUser.role === 'admin'){
            this.isAdmin = true;
            this.loadData(this.accountId);
        }
        
    }

    public tabChange = (evt: any) =>{
        let clickedTabId = evt.nextId;
        if(clickedTabId === 'tab-0'){
            this.mode = 'modules';
            this.accountsModuleList.loadData(1, this.accountId);
        }
        else if(clickedTabId === 'tab-1'){
            this.mode = 'users';
            this.usersList.isAdmin = this.isAdmin;
            this.usersList.isSuperAdmin = this.isSuperAdmin;
            this.usersList.loadData(1, this.accountId);
        }
        else if(clickedTabId === 'tab-2'){
            this.mode = 'objects';
            this.sitesList.isAdmin = this.isAdmin;
            this.sitesList.loadData(1, this.accountId);
        }
    }

    public loadData(id: number) : void{
        this.showSpinner = true;
        this.accountService.getAccount(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((data) => {
            this.model = data.Data;
        });
    }

    public accountSubModulesMode() : void {
        if(this.mode === 'users') {
            this.usersList.isAdmin = this.isAdmin;
            this.ngAccountTab.select('tab-1');
            this.usersList.loadData(1, this.accountId);
        }
        else if(this.mode === 'objects'){
            this.sitesList.isAdmin = this.isAdmin;
            this.ngAccountTab.select('tab-2');
            this.sitesList.loadData(1, this.accountId);
        }
        else if(this.mode === 'modules') {
            this.ngAccountTab.select('tab-0');   
            this.accountsModuleList.loadData(1, this.accountId);
        }
    }
    public onIsActiveChange(value: boolean){
        this.model.IsActive = value;
    }

    public onIsDeletedChange(value: boolean){
        this.model.IsDeleted = value;
    } 

    public saveChanges() : void{
        this.accountService.saveAccount(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
            } 
            this.ngOnInit();
        });
    }

    public delete() : void{
        this.accountService.deleteAccount(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                setTimeout(() => {
                    this.router.navigate([`/accounts`]);
                }, 1000);
            } 
            this.ngOnInit();
        });
    }

    public createUser(id) : void{
         this.router.navigate([`users/create/${id}`]);  
    }

    public createSite(id) : void{
         this.router.navigate([`objects/create/${id}`]);  
    }
        
    public confirmDelete(content: string): void {
        this.modalService.open(content, { size: 'sm' });
    }
}
