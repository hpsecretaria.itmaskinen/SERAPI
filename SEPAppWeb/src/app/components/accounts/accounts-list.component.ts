import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { routerTransition } from '../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { AccountService } from '../../services';

@Component({
    selector: 'app-accounts-list',
    templateUrl: './accounts-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class AccountsListComponent implements OnInit {
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public showSpinner: boolean = false;
    constructor(private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private accountService: AccountService) {}

    packageData: Subject<any> = new Subject<any[]>();   

    public ngOnInit() {
        this.loadData();
    }

    public editAccount(id:number){
        this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
    }
    public editAccountModule(id:number){
        this.router.navigate([`${id}/edit/modules`], { relativeTo: this.route });  
    }
    public editAccountUser(id:number){
        this.router.navigate([`${id}/edit/users`], { relativeTo: this.route });  
    }

    public editAccountSite(id:number){
        this.router.navigate([`${id}/edit/objects`], { relativeTo: this.route });  
    }

    public loadData(page?: number){
        this.showSpinner = true;
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 
        this.accountService.getAccountsList(this.currentPage, this.itemPerPage, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
            //this.toastr.success(response.Message, "Success"); 
        });
    }
    
}
