import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { ToastsManager } from 'ng2-toastr';
import { CommonService } from '../../../services';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public model: {
            AccountsCount : 0,
            ActiveUsersCount : 0,
            InActiveUsersCount : 0,
            SitesCount : 0
        };

    //CHARTS
// bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public barChartLabels: string[] = [
        '2006',
        '2007',
        '2008',
        '2009',
        '2010',
        '2011',
        '2012'
    ];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;

    public barChartData: any[] = [
        { data: [65, 59, 80, 81, 56, 55, 40, 90], label: 'Unibail Rodamco' }
    ];

    public accountUsersData : number [] = [];
    public accountUserLabels: string [] = []; 
    
    public logCountersData : number [] = [];
    public logCountersLabels: string [] = ['Information', 'Warning', 'Error', 'Notification']; 
     
    // Doughnut
    public doughnutChartLabels: string[] = [];
    public doughnutChartData: number[] = [];
    public doughnutChartType: string = 'doughnut';
    public doughnutChartOptions: any = {
                    legend: {
                        display: false
                    },
                    tooltips: {
                        displayColors:false,
                        callbacks: {
                            label : ((tooltipItem) => {
                                let index = tooltipItem.index;
                                let label = this.doughnutChartLabels[index];
                                return label;
                            })
                        }
                    }
                }
    //CHARTS

    constructor(
        private router: Router,
        private commonService: CommonService, 
        private toastr : ToastsManager) {
            
    }

    public ngOnInit() {
        this.model = {
            AccountsCount : 0,
            ActiveUsersCount : 0,
            InActiveUsersCount : 0,
            SitesCount : 0
        };

        this.logCountersData = [0, 0, 0, 0];
        //only enable this module for superadmin
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser.role !== 'superadmin'){
            this.router.navigate(['/objects']);
            return false;
        }
        this.loadData();
        this.loadLogStatistics();
    }

    public loadData(){
        let filter = '';
        this.commonService.getDashboardCounters(this.currentPage, this.itemPerPage, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            const data: Array<any> = response.Data.AccountUserCounter;
            
            data.forEach((item: any) => {
                let counter = item.TotalUserCount == 0 ? 1 : item.TotalUserCount
                this.doughnutChartLabels.push(`${item.AccountName} (${counter})`);
                this.doughnutChartData.push(counter);
            });

            //this.toastr.success(response.Message, "Success"); 
        });
    }

    public loadLogStatistics(){
        this.commonService.getDashboardLogCounters()
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            let data = response.Data;
            this.logCountersData = [data.InformationCount, data.WarningCount, data.ErrorCount, data.NotifificationCount];
        });
    }

    public chartHovered(event:any){
        
    }
}
