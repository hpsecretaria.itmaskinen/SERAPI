import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { DataService, UtilityService } from '../../core/services';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { SpinnerComponent } from '../../shared/modules/spinner/spinner.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { ToastModule, ToastOptions } from 'ng2-toastr'; 
import { NotficationOptions } from '../../core/options/toastr.options';

import { 
    ModulesComponent, ModulesListComponent,
    AccountsComponent, AccountsCreateComponent, AccountsListComponent, AccountsEditComponent,
    AccountModuleComponent, AccountsModuleListComponent,
    UserModuleRoleComponent,
    UsersComponent, UsersCreateComponent, UsersListComponent, UsersEditComponent,
    SitesComponent, SitesCreateComponent, SitesEditComponent, SitesListComponent,
    TenantsComponent, TenantsListComponent, TenantsCreateComponent, TenantsEditComponent,
    LoansComponent, LoansListComponent, LoansPreviewComponent, LoansCreateComponent, LoansEditComponent,
    //MOBILE
    MobileLoansCreateComponent, MobileLoansPreviewComponent, MobileLoansLandingComponent, 
    MobileLoansListComponent, MobileLoansReturnComponent, CustomTypeComponent, 
    CustomTypeCreateComponent, CustomTypeListComponent, CustomTypeEditComponent, 
    GuardComponent, GuardListComponent, GuardEditComponent, IncidentsComponent, 
    IncidentsEditComponent, IncidentsListComponent, IncidentsCreateComponent, 
    IncidentTypesCreateComponent, IncidentTypesComponent, IncidentTypesEditComponent, 
    IncidentTypesListComponent, TagLogsComponent, TagLogsCreateComponent, TagLogsEditComponent, 
    TagLogsListComponent, TagsComponent, TagsListComponent, TagsEditComponent, TagsCreateComponent
} from '../../components';

import {
    ModuleService, AccountService, UserService, ManagerService, CommonService, 
    SiteService, AuthenticationService, UserModuleRoleService, TenantService, LoanService, CustomTypeService, GuardService
} from '../../services'

// Pipes
import { DateFormatPipe, UppercasePipe, LocalDateFormatPipe, NullableBooleanPipe } from '../../shared/pipes';
import { IncidentService } from '../../services/incident.service';
import { IncidentTypeService } from '../../services/incident-type.service';
import { TagLogService } from '../../services/tag-log.service';
import { TagService } from '../../services/tag.service';
import { GuardCreateComponent } from '../guards/guard-create.component';
import { CalendarModule, PickListModule, GrowlModule } from 'primeng/primeng';


@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbModule,
        NgbModule.forRoot(),
        NgxPaginationModule,
        ToastModule.forRoot(),
        CalendarModule,   
        PickListModule,
        GrowlModule     
    ],
    declarations: [
        SpinnerComponent,
        LayoutComponent, SidebarComponent, HeaderComponent,
        ModulesComponent, ModulesListComponent,
        AccountsComponent, AccountsCreateComponent, AccountsEditComponent, AccountsListComponent,
        AccountModuleComponent, AccountsModuleListComponent,
        UserModuleRoleComponent,
        UsersComponent, UsersCreateComponent, UsersListComponent, UsersEditComponent,
        SitesComponent, SitesCreateComponent, SitesListComponent, SitesEditComponent,
        TenantsComponent, TenantsListComponent, TenantsCreateComponent, TenantsEditComponent,
        LoansComponent, LoansListComponent, LoansPreviewComponent, LoansCreateComponent, LoansEditComponent,
        //MOBILE
        MobileLoansCreateComponent, MobileLoansPreviewComponent, MobileLoansLandingComponent, 
        MobileLoansListComponent, MobileLoansReturnComponent,
         // Pipes
        DateFormatPipe,
        UppercasePipe,
        LocalDateFormatPipe,
        NullableBooleanPipe,

        CustomTypeComponent,CustomTypeCreateComponent,CustomTypeListComponent,CustomTypeEditComponent,
        GuardComponent,GuardListComponent,GuardEditComponent,GuardCreateComponent ,
        IncidentsComponent,IncidentsEditComponent,IncidentsListComponent,IncidentsCreateComponent,
        IncidentTypesComponent,IncidentTypesCreateComponent,IncidentTypesEditComponent,IncidentTypesListComponent,
        TagLogsComponent,TagLogsCreateComponent,TagLogsEditComponent,TagLogsListComponent,
        TagsComponent,TagsListComponent,TagsEditComponent,TagsCreateComponent,

    ],
     providers: [
         DataService, 
         UtilityService,
         ModuleService,
         AccountService,
         UserService,
         ManagerService,
         CommonService,
         SiteService,
         AuthenticationService,
         UserModuleRoleService,
         TenantService,
         LoanService,
         CustomTypeService,
         GuardService,
         IncidentTypeService,
         IncidentService,
         TagLogService,
         TagService,
         { provide: ToastOptions, useClass: NotficationOptions },
     ]
})
export class LayoutModule {}
