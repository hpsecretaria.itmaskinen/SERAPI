import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ModulesListComponent } from './modules-list.component';
@Component({
    selector: 'app-modules',
    templateUrl: './modules.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ModulesComponent implements OnInit {
    @ViewChild('modulesList') modulesList: ModulesListComponent;
    constructor() {}

    ngOnInit() {}
}
