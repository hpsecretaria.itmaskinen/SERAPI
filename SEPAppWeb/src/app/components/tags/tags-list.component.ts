import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { TagService } from '../../services/tag.service';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.scss']
})
export class TagsListComponent extends BaseComponent implements OnInit {
  public showSpinner: boolean = false;
  public itemPerPage: number = 10;
  public currentPage: number = 1;
  public totalItems: number = 10;
  public isAdminOrUserEditor = false;
  public isSuperAdmin = false;
  public isPreviewOn: boolean = false;
  //public parentComponent : any;
  constructor(
  //  private parentInj: Injector,
    private router: Router,
    private route:ActivatedRoute, 
    private toastr: ToastsManager,
    private tagService: TagService,
    private location: Location)  {
        super(location, router, route)
    }
    packageData: Subject<any> = new Subject<any[]>();  

  ngOnInit() 
  {
    if(this.isSuperAdmin  && !super.secondaryAdminUser())
    {        
        this.loadData();
    }
  }

  public loadData(page?: number, id?: number){
    this.showSpinner = true;
    let filter = '';
    if (page != null) {
        this.currentPage = page; 
    } 

    this.tagService.getAllTags()
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
       // this.toastr.success(response.Message, "Success"); 
    });    
  }

  public loadDataById(id:number){
    this.showSpinner = true;
     this.tagService.getAllTags(id)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
      //  this.toastr.success(response.Message, "Success"); 
    }); 
}

public editTag(id:number){
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
}



}
