import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { IncidentService } from '../../services/incident.service';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
import { IncidentModel } from '../../models';

@Component({
  selector: 'app-incidents-list',
  templateUrl: './incidents-list.component.html',
  styleUrls: ['./incidents-list.component.scss']
})
export class IncidentsListComponent extends BaseComponent  implements OnInit {

  public showSpinner: boolean = false;
  public itemPerPage: number = 10;
  public currentPage: number = 1;
  public totalItems: number = 10;
  public isAdminOrUserEditor = false;
  public isSuperAdmin = false;5
  public isPreviewOn: boolean = false;
 // public parentComponent : any;
  constructor(
   // private parentInj: Injector,
    private router: Router,
    private route:ActivatedRoute, 
    private toastr: ToastsManager,
    private incidentService: IncidentService, 
    private location: Location)  {
        super(location, router, route)
    }
    packageData: Subject<any> = new Subject<any[]>();  

  ngOnInit() 
  {
    if(this.isSuperAdmin && !super.secondaryAdminUser())
    {        
        this.loadData();
    }
  }

  public loadData(page?: number, id?: number){
    this.showSpinner = true;
    let filter = '';
    if (page != null) {
        this.currentPage = page; 
    } 

    this.incidentService.getAllIncidents(0)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        
        this.packageData.next(response.Data);
       
        this.totalItems = response.Data.length;
     //   this.toastr.success(response.Message, "Success"); 
    });    
  }

  public loadDataById(id:number){
    this.showSpinner = true;
     this.incidentService.getAllIncidents(id)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
     //   this.toastr.success(response.Message, "Success"); 
    }); 
}

public editIncident(id:number){
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
}

public getIfReportRequired(item: any)
{
    if(item.IncidentType.IsReportRequired){     
        var requiredReports = item.IncidentType.RequiredReport;

        if(requiredReports.length == 1){
            if(requiredReports[0].Id == 0){
                return false;
            }
            
        }

        return !requiredReports.some(e=> item.Reports.some(r=>r.ReportTypeId == e.Id))  
    }else{
        return  false;
    }
};

}
