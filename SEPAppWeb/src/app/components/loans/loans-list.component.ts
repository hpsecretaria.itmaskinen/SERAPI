import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

import { ToastsManager } from 'ng2-toastr';

import { LoanService } from '../../services';
import { BaseComponent } from '../../core/components';
import { LoansComponent } from '../loans/loans.component';
@Component({
    selector: 'app-loans-list',
    templateUrl: './loans-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class LoansListComponent extends BaseComponent implements OnInit {
    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    public isPreviewOn: boolean = false;
    public parentComponent : any;
    constructor( 
        private parentInj: Injector,
        private toastr: ToastsManager,
        private loanService: LoanService,
        public router: Router,
        private route: ActivatedRoute,
        private location: Location){
            super(location, router, route)
            this.parentComponent = this.parentInj.get(LoansComponent);
        }

    packageData: Subject<any> = new Subject<any[]>();  

    public ngOnInit() {
        if(this.isSuperAdmin && !super.secondaryAdminUser())
        {
            this.loadData();
        }
    }

    public loadData(page?: number, id?: number){
        this.showSpinner = true;
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 

        this.loanService.getLoanList(this.currentPage, this.itemPerPage, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
            //this.toastr.success(response.Message, "Success"); 
        });
        
    }

    public loadDataById(id:number){
        this.showSpinner = true;
        this.loanService.getLoanByAccountId(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }

    public editLoan(id:number){
        this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
    }

    public onPreview(data:any){
        this.isPreviewOn = true;
        this.parentComponent.isPreviewOn = true;
        this.parentComponent.listCol = 'col-md-7';
        this.parentComponent.loanPreviewData = data;
        this.parentComponent.loadLoanPreview(data);
    }
    
}
