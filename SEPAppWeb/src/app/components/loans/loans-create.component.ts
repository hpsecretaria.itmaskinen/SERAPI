import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoansListComponent } from './loans-list.component';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { LoanModel } from '../../models';
import { LoanService, TenantService, SiteService } from '../../services';

import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
    selector: 'app-loans-create',
    templateUrl: './loans-create.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class LoansCreateComponent implements OnInit {
    @ViewChild('imagePreview') imagePreviewEl: ElementRef;
    showSpinner: boolean = false;
    isAdminOrUserEditor: boolean;
    model : any;
    currentUser: any;
    tenantsList : any;
    customersList: any;
    sitesList: any;
    loanTypesList: any;
    loanedDate: NgbDateStruct;
    returnedDate: NgbDateStruct;
    defaultDaysReturn: number = 30; //1 MONTH FROM LOANING
    photoSrc: string = "";
    public finalCustomerList: any;
    constructor(
        private loanService : LoanService,
        private tenantService : TenantService,
        private siteService: SiteService,
        private toastr: ToastsManager
    ) {}

    public ngOnInit() {
        this.model = new LoanModel();
        this.setDefaults();
        this.loadAllMergedCustomers();
        this.loadSites(this.currentUser.accountId);
        this.loadLoanTypes();
    }

    public setDefaults(){
        let now = new Date();
        this.photoSrc = "/assets/images/upload-empty.png";
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(this.currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(this.currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }

        let monthAfter = new Date(now.setDate(now.getDate() + this.defaultDaysReturn));
        this.loanedDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        this.returnedDate = { year: monthAfter.getFullYear(), month: monthAfter.getMonth() + 1, day: monthAfter.getDate() }

        this.model.ReceivedBy = this.currentUser.displayName;
        this.model.CreatedBy = this.currentUser.displayName;
        this.model.LastModifiedBy = this.currentUser.displayName;
    }

    public loadSites(id? :number){
        this.siteService.getSitesByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            this.sitesList = response.Data;
        });
    }

    public loadLoanTypes(){
        this.loanService.getLoanTypes()
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            this.loanTypesList = response.Data;
        });
    }

    public loadAllMergedCustomers(){
        this.loanService.getAllMergedCustomerList(this.currentUser.accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            this.finalCustomerList = response.Data;
        });
    }

    public loadTenant(id){
        this.tenantService.getTenant(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            let tenantData = response.Data;
            this.model.CustomerTypeId = 2;

            let city = tenantData.InvoiceZipAndCity && tenantData.InvoiceZipAndCity.indexOf(',') > -1 ? tenantData.InvoiceZipAndCity.split(",")[1] : '';
            let zipCode = tenantData.InvoiceZipAndCity && tenantData.InvoiceZipAndCity.indexOf(',') > -1 ? tenantData.InvoiceZipAndCity.split(",")[0] : '';

            this.model.Customer = {
                Name : tenantData.Name,
                Company: tenantData.Name,
                Email: tenantData.Email,
                MobilePhone : tenantData.Phone,
                Address1 : tenantData.VisitingAddress,
                City : city,
                ZipCode : zipCode
            };

            this.model.GivenBy = tenantData.SiteManagerName;
        });
        
    }

    public loadCustomer(id){
        this.loanService.getCustomer(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            this.model.Customer = response.Data;
            this.model.GivenBy = response.Data.Name;
            this.model.CustomerTypeId = 1;
        });
    }

    public selectCustomer(customer){
        let selectedCustomer = this.finalCustomerList.filter((item) => {
            return item.Name === customer;
        })[0];
        this.model.CustomerId = selectedCustomer.Id;
        this.loadData(selectedCustomer.Id, selectedCustomer.Type);
    }

    public loadData(id, type){
        if(type === 'Customer')
        {
            this.loadCustomer(id)
        }
        else {
            this.loadTenant(id);
        }
    }

    public saveChanges(){
        this.showSpinner = true;
        this.model.LoanedDate = new Date(
            this.loanedDate.year, 
            this.loanedDate.month, 
            this.loanedDate.day);

        this.model.ReturnedDate = new Date(
            this.returnedDate.year, 
            this.returnedDate.month, 
            this.returnedDate.day);

        this.loanService.saveLoan(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                this.ngOnInit();
            }
            
        });
    }

    public fileChange(fileInput: any){
        let self = this;
        let file = <File>fileInput.target.files[0];
         if(file.size > 2000000)
        {
            this.toastr.error(`Photo upload limits 2MB of file, this file is ${ (file.size / 1024 / 1024).toFixed(2) } KB. Please choose another one.`, "Error");
            return false;
        }
        var reader = new FileReader();
        reader.onload = function () {
            self.photoSrc = reader.result;
            self.model.PhotoString = reader.result.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };

        reader.readAsDataURL(file);
    }
    
}
