Was able to finish modifying the tests so that it runs using browserstack
on all the browsers available. IE, Chrome, Safari, Firefox and Edge

I also modified the test to close all session when the test fails, last time
it was still continuing the session even if the test fails. it happens both
 on local browser and browsertack.

Found an issue last time that whenever running the level 4 test it shows the 
pennsylvaina MVR release form even though Alabama was selected and
 it also had the old signature input. I already informed
RJ last Friday. earlier he said he was able to fix it. I will have  to sync my code
and test it after the meeting.


I will also still have to modify again the test for the applicant link default workflow to 
select Level 3 instead of level 1
