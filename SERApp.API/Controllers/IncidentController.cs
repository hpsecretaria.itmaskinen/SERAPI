﻿using Hangfire;
using Hangfire.Storage;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.API.SSRSExecution;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentController : ApiController
    {
        private IncidentService _service;
        private AccountService _accountService;
        private IncidentTypeService _incidentTypeService;
        private DailyReportRecipientService _dailyReportRecipientService;
        private EmailService _emailService;
        private SiteService _siteService;
        private ReportTypeScheduleService _reportTypeScheduleService;
        private ModuleService _moduleService;

        public IncidentController()
        {
            _service = new IncidentService();
            _accountService = new AccountService();
            _incidentTypeService = new IncidentTypeService();
            _dailyReportRecipientService = new DailyReportRecipientService();
            _emailService = new EmailService();
            _siteService = new SiteService();
            _reportTypeScheduleService = new ReportTypeScheduleService();
            _moduleService = new ModuleService();
        }

        [HttpGet]
        [Route("Incident/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetAll")]
        public IHttpActionResult GetAll(int accountId =0,int siteId=0,string selectedDate = "",int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidents(accountId,siteId,selectedDate,selectedType1,selectedType2,selectedType3, searchText);
                return new ResponseDataModel<IEnumerable<IncidentListModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetTimezones")]
        public IHttpActionResult GetTimezones()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var tzis = TimeZoneInfo.GetSystemTimeZones();
                var arrTimeZoneInfo = new List<TimeZoneInfo>();
                foreach (var tzi in tzis)
                {
                    arrTimeZoneInfo.Add(tzi);
                }

                return new ResponseDataModel<List<TimeZoneInfo>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = arrTimeZoneInfo
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetGuardsToday")]
        public IHttpActionResult GetGuardsToday(int accountId = 0, int siteId = 0, string selectedDate = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetGuardsToday(accountId, siteId, selectedDate);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Incident/GetIncidentRange")]
        public IHttpActionResult GetIncidentRange([FromBody]WeekReportModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidentRange(model);
                return new ResponseDataModel<WeekReportOverallReportModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }
        
        [HttpPost]
        [Route("Incident/Save")]
        public IHttpActionResult Save(IncidentModel model)
        {   
            return Ok(this.ConsistentApiHandling(() =>
            {
                var incident = this._service.GetIncident(_service.AddIncident(model));
                var account = this._accountService.Get(incident.AccountId);
                var moduleId = model.Reports.FirstOrDefault()?.moduleId;

                //incident.Reports.ForEach(report =>
                //{
                    //var jobId = $"{account.Name} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                    //var sendReport = new SendReport();
                    //sendReport.Send(incident.Id, report.Id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));
                    //RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.Send(incident.Id, report.Id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump")), schedule.Cron);
                    //var schedule = this._reportTypeScheduleService.Get(model.AccountId, report.ReportTypeId, (int)moduleId);
                    //if (schedule != null)
                    //{
                    //    var module = this._moduleService.Get((int)moduleId);
                    //    var jobId = $"{account.Name} {module.Title} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                    //    RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.Send(incident.Id, report.Id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump")), schedule.Cron);
                    //}
                //});

                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Incident",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetAllSection")]
        public IHttpActionResult GetAllSection(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSectionModels().Where(r=>r.SiteId == siteId).OrderBy(x=>x.SectionValue);
                return new ResponseDataModel<IEnumerable<SectionModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Sections",
                    Data = data
                };
            }));
        }
      
        [HttpPost]
        [Route("Incident/Delete")]
        public IHttpActionResult Delete(int id, [FromUri] int moduleId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var incident = this._service.GetIncident(id);
                var account = this._accountService.Get(incident.AccountId);
                var module = this._moduleService.Get((int)moduleId);

                incident.Reports.ForEach(report =>
                {
                    var schedule = this._reportTypeScheduleService.Get(incident.AccountId, report.ReportTypeId, moduleId);
                    if (schedule != null)
                    {
                        var jobId = $"{account.Name} {module.Title} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                        RecurringJob.RemoveIfExists(jobId);
                    }
                });

                _service.DeleteIncident(id);
                
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident",
                };
            }));
        }

        [HttpPost]
        [Route("Incident/SendDailyReport")]
        public IHttpActionResult SendDailyReport([FromBody]ReportEmailModel base64Data)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var sendReport = new SendReport();
                switch (base64Data.routineName)
                {
                    case "WeeklyReport":
                        var date1 = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
                        var date2 = new DateTime(base64Data.Date2.year, base64Data.Date2.month, base64Data.Date2.day);

                        var accountId = _siteService.Get(base64Data.siteId).AccountId;
                        sendReport.SendDailyReport(date2, date1, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountId,_dailyReportRecipientService);
                        break;
                    case "DailyReport":
                        var accountId2 = _siteService.Get(base64Data.siteId).AccountId;
                        var newDate = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
                            sendReport.SendDailyReport(newDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountId2, _dailyReportRecipientService);
                        break;
                    case "MonthReport":
                        var date1MonthReport = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
                        var date2MonthReport = new DateTime(base64Data.Date2.year, base64Data.Date2.month, base64Data.Date2.day);

                        var accountIdMonthReport = _siteService.Get(base64Data.siteId).AccountId;
                        sendReport.SendDailyReport(date1MonthReport, date2MonthReport, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountIdMonthReport, _dailyReportRecipientService);
                        break;
                    default:
                        var incident = _service.GetIncident(base64Data.IncidentId);
                        var account = _accountService.Get(incident.AccountId);

                        incident.Reports.ForEach(report =>
                        {
                            var jobId = $"{account.Name} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                           // var sendReport = new SendReport();
                            sendReport.Send(incident.Id, report.Id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));

                        });
                        break;
                }
                
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Sent Daily Report",
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetExcel")]
        public HttpResponseMessage GetExcel()
        {
            var path = @"C:\Users\lemon\Desktop\New folder (4)\EXPENSES.xlsx";
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            //result.Content.Headers.ContentType =
            //    new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        [HttpGet]
        [Route("Incident/DownloadReport")]
        public HttpResponseMessage DownloadReport(string routineName, string date1data, string date2data, int siteId)
        {
            var sendReport = new SendReport();
            switch (routineName)
            {
                case "WeeklyReport":
                    var date1 = Convert.ToDateTime(date1data);
                    var date2 = Convert.ToDateTime(date2data);

                    var accountId = _siteService.Get(siteId).AccountId;
                    return sendReport.DownloadDailyReport(date2, date1, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, accountId, _dailyReportRecipientService);

                case "DailyReport":
                    var accountId2 = _siteService.Get(siteId).AccountId;
                    var newDate = Convert.ToDateTime(date1data);
                    return sendReport.DownloadDailyReport(newDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, accountId2, _dailyReportRecipientService);

                case "MonthReport":
                    var date1MonthReport = Convert.ToDateTime(date1data);
                    var date2MonthReport = Convert.ToDateTime(date2data);
                    var dd1 = new DateTime(date1MonthReport.Year, date1MonthReport.Month,1);
                    var dd2 = dd1.AddMonths(1).AddDays(-1);
                    var accountIdMonthReport = _siteService.Get(siteId).AccountId;
                    return sendReport.DownloadDailyReport(dd1, dd2, "MonthReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, accountIdMonthReport, _dailyReportRecipientService);

                default:
                    return new HttpResponseMessage();

            }

            //var sendReport = new SendReport();
            //switch (base64Data.routineName)
            //{
            //    case "WeeklyReport":
            //        var date1 = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
            //        var date2 = new DateTime(base64Data.Date2.year, base64Data.Date2.month, base64Data.Date2.day);

            //        var accountId = _siteService.Get(base64Data.siteId).AccountId;
            //        return sendReport.DownloadDailyReport(date2, date1, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountId, _dailyReportRecipientService);

            //    case "DailyReport":
            //        var accountId2 = _siteService.Get(base64Data.siteId).AccountId;
            //        var newDate = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
            //        return sendReport.DownloadDailyReport(newDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountId2, _dailyReportRecipientService);

            //    case "MonthReport":
            //        var date1MonthReport = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
            //        var date2MonthReport = new DateTime(base64Data.Date2.year, base64Data.Date2.month, base64Data.Date2.day);

            //        var accountIdMonthReport = _siteService.Get(base64Data.siteId).AccountId;
            //        return sendReport.DownloadDailyReport(date1MonthReport, date2MonthReport, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountIdMonthReport, _dailyReportRecipientService);

            //    default:
            //        return new HttpResponseMessage();

            //}

        }


        [HttpPost]
        [Route("Incident/EnableSchedule")]
        public IHttpActionResult EnableSchedule(dynamic job)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _reportTypeScheduleService.ToggleEnable(job);

                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident",
                };
            }));
        }
    }

    public class SendReport
    {
        public void SchedulerReport(DateTime date1, DateTime date2, string reportType, string dumpPath, int siteId, int accountId = 0, DailyReportRecipientService dailyReportRecipientService = null, string jobId = "")
        {
            try
            {
                var reportTypeScheduleService = new ReportTypeScheduleService();
                var isEnabled = reportTypeScheduleService.GetByJobId(jobId).IsEnabled;

                if (!isEnabled)
                {
                    return;
                }

                var ssrsEndPoint = ConfigurationManager.AppSettings["SSRSEndPoint"];

                var res = new ReportExecutionService();

                if (ssrsEndPoint.Contains("localhost"))
                {
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                }
                else
                {
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                }

                //
                res.Url = $"{ssrsEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;

                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                var parameters = new List<ParameterValue>();
                var reportPath = "";
                switch (reportType)
                {
                    case "WeeklyReport":
                        reportPath = $"/SER4/Week Report Template/Week Report Main";

                        var monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                        date1 = monday;
                        var sunday = monday.AddDays(6);

                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = monday.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = sunday.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "DailyReport":
                        date1 = DateTime.Now;
                        reportPath = $"/SER4/Daily Report Template/Daily Report";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "MonthReport":
                        date1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        reportPath = $"/SER4/Month Report Template/Month Report Main";
                        date2 = date1.AddMonths(1).AddDays(-1);
                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;

                }

                var execInfo = res.LoadReport(reportPath, historyId);
                var extension = string.Empty;
                var encoding = string.Empty;
                var mimeType = string.Empty;
                Warning[] warnings;
                string[] streamsIds;

                res.SetExecutionParameters(parameters.ToArray(), "en-US");

                var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                execInfo = res.GetExecutionInfo();


                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                var tt = Guid.NewGuid().ToString();
                var path = Path.Combine(dumpPath, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}");

                var stream = File.Create(path, result.Length);
                stream.Write(result, 0, result.Length);
                stream.Close();

                //var reportType = report.ReportType;


                var emails = dailyReportRecipientService.GetDailyReportRecipients(accountId, siteId, reportType).ToList();
                //emails.Add(new DailyReportRecipientModel
                //{
                //    Email = "clydeburgos@gmail.com"
                //});

                var reportEmailModel = new ReportEmailModel
                {
                    recepients = emails.Select(e => e.Email).ToArray(),
                    EmailTitle = reportType,
                    routineName = reportType,
                    siteId = siteId
                };

                var file = File.OpenRead(path);
                this.Send(reportEmailModel, file, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}.pdf");
                file.Close();

                //delete the file
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                var test = "";
            }

        }

        public void SendDailyReport(DateTime date1,DateTime date2, string reportType, string dumpPath, int siteId,int accountId =0, DailyReportRecipientService dailyReportRecipientService = null)
        {
            try
            {

                var ssrsEndPoint = ConfigurationManager.AppSettings["SSRSEndPoint"];

                var res = new ReportExecutionService();

                if (ssrsEndPoint.Contains("localhost"))
                {
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                }
                else {
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                }
                
                //
                res.Url = $"{ssrsEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;
               
                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                var parameters = new List<ParameterValue>();
                var reportPath = "";
                switch (reportType)
                {
                    case "WeeklyReport":
                        reportPath = $"/SER4/Week Report Template/Week Report Main";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "DailyReport":

                        reportPath = $"/SER4/Daily Report Template/Daily Report";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "MonthReport":
                        date1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        reportPath = $"/SER4/Month Report Template/Month Report Main";
                        date2 = date1.AddMonths(1).AddDays(-1);
                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                }

                var execInfo = res.LoadReport(reportPath, historyId);
                var extension = string.Empty;
                var encoding = string.Empty;
                var mimeType = string.Empty;
                Warning[] warnings;
                string[] streamsIds;

                res.SetExecutionParameters(parameters.ToArray(), "en-US");

                var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                execInfo = res.GetExecutionInfo();


                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                var tt = Guid.NewGuid().ToString();
                var path = Path.Combine(dumpPath, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}");

                var stream = File.Create(path, result.Length);
                stream.Write(result, 0, result.Length);
                stream.Close();

                //var reportType = report.ReportType;

                
                var emails = dailyReportRecipientService.GetDailyReportRecipients(accountId, siteId, reportType).ToList();
                //emails.Add(new DailyReportRecipientModel
                //{
                //    Email = "clydeburgos@gmail.com"
                //});

                var reportEmailModel = new ReportEmailModel
                {
                    recepients = emails.Select(e => e.Email).ToArray(),
                    EmailTitle = reportType,
                    routineName = reportType,
                    siteId = siteId
                };

                var file = File.OpenRead(path);
                this.Send(reportEmailModel, file, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}.pdf");
                file.Close();

                //delete the file
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch(Exception ex)
            {
                var test = "";
            }


        }
        public HttpResponseMessage DownloadDailyReport(DateTime date1, DateTime date2, string reportType, string dumpPath, int siteId, int accountId = 0, DailyReportRecipientService dailyReportRecipientService = null)
        {
            try
            {

                var ssrsEndPoint = ConfigurationManager.AppSettings["SSRSEndPoint"];

                var res = new ReportExecutionService();

                if (ssrsEndPoint.Contains("localhost"))
                {
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                }
                else
                {
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                }

                //
                res.Url = $"{ssrsEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;

                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                var parameters = new List<ParameterValue>();
                var reportPath = "";
                switch (reportType)
                {
                    case "WeeklyReport":
                        reportPath = $"/SER4/Week Report Template/Week Report Main";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "DailyReport":

                        reportPath = $"/SER4/Daily Report Template/Daily Report";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "MonthReport":
                        date1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        reportPath = $"/SER4/Month Report Template/Month Report Main";
                        date2 = date1.AddMonths(1).AddDays(-1);
                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                }

                var execInfo = res.LoadReport(reportPath, historyId);
                var extension = string.Empty;
                var encoding = string.Empty;
                var mimeType = string.Empty;
                Warning[] warnings;
                string[] streamsIds;

                res.SetExecutionParameters(parameters.ToArray(), "en-US");

                var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                execInfo = res.GetExecutionInfo();


                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                var tt = Guid.NewGuid().ToString();
                var path = Path.Combine(dumpPath, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}");

                var stream = File.Create(path, result.Length);
                stream.Write(result, 0, result.Length);
                stream.Close();

                var emails = dailyReportRecipientService.GetDailyReportRecipients(accountId, siteId, reportType).ToList();
              
                var reportEmailModel = new ReportEmailModel
                {
                    recepients = emails.Select(e => e.Email).ToArray(),
                    EmailTitle = reportType,
                    routineName = reportType,
                    siteId = siteId
                };

                var file = File.OpenRead(path);

                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(file);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "file";
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;
                //this.Send(reportEmailModel, file, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}.pdf");
                //file.Close();

                ////delete the file
                //if (File.Exists(path))
                //{
                //    File.Delete(path);
                //}
            }
            catch (Exception ex)
            {
               
                var test = "";
                return null;
            }
        }

        public void Send(int id, int reportId, string dumpPath)
        {
            var incidentService = new IncidentService();
            var dailyReportRecipientService = new DailyReportRecipientService();

            var model = incidentService.GetIncident(id);
            var report = model.Reports.FirstOrDefault(e => e.Id == reportId);

            if (report != null)
            {
                var ssrsEndPoint = ConfigurationManager.AppSettings["SSRSEndPoint"];

                var res = new ReportExecutionService();

                if (ssrsEndPoint.Contains("localhost"))
                {
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                }
                else
                {
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                }

                res.Url = $"{ssrsEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;
                var reportPath = $"/SER4/{report.ReportType.Name}";
                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                var parameters = new List<ParameterValue>();

                switch (report.ReportTypeId)
                {
                    case 1:
                        reportPath = "/SER4/Handelserapport Template/Handelserapport";
                        parameters.Add(new ParameterValue
                        {
                            Name = "ReportId",
                            Value = $"{report.Id}"
                        });
                        break;
                    case 2:
                        reportPath = "/SER4/IB-Larm och Ryckrapport Template/IB-Larm-och-Ryckrapport";
                        parameters.Add(new ParameterValue
                        {
                            Name = "ReportId",
                            Value = $"{report.Id}"
                        });
                        break;
                    case 3:
                        reportPath = "/SER4/Öppettider Template/Öppettider Main";
                        parameters.Add(new ParameterValue
                        {
                            Name = "ReportId",
                            Value = $"{report.Id}"
                        });
                        break;
                    case 4:
                        reportPath = "/SER4/Skadegorelse Klotterrapport Template/SkadegorelseKlotterrapport";
                        parameters.Add(new ParameterValue
                        {
                            Name = "ReportId",
                            Value = $"{report.Id}"
                        });
                        break;
                    case 5:
                        reportPath = "/SER4/Viten Template/Viten";
                        parameters.Add(new ParameterValue
                        {
                            Name = "ReportId",
                            Value = $"{report.Id}"
                        });
                        break;
                    case 7:
                        reportPath = "/SER4/Bistro Solna Template/Bistro Solna";
                        parameters.Add(new ParameterValue
                        {
                            Name = "ReportId",
                            Value = $"{report.Id}"
                        }); break;
                    default:
                        return;

                }

                try
                {
                    var execInfo = res.LoadReport(reportPath, historyId);
                    var extension = string.Empty;
                    var encoding = string.Empty;
                    var mimeType = string.Empty;
                    Warning[] warnings;
                    string[] streamsIds;

                    res.SetExecutionParameters(parameters.ToArray(), "en-US");

                    var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                    execInfo = res.GetExecutionInfo();


                    if (!Directory.Exists(dumpPath))
                    {
                        Directory.CreateDirectory(dumpPath);
                    }

                    var tt = Guid.NewGuid().ToString();
                    var path = Path.Combine(dumpPath, $"{report.ReportType.Name.Replace("/","")}{id}{tt}");

                    var stream = File.Create(path, result.Length);
                        stream.Write(result, 0, result.Length);
                        stream.Close();

                    var reportType = report.ReportType;
               
                    var emails = dailyReportRecipientService.GetDailyReportRecipients(model.AccountId, model.SiteId, reportType.Name).ToList();
                    //emails.Add(new DailyReportRecipientModel
                    //{
                    //    Email = "clydeburgos@gmail.com"
                    //});

                    var reportEmailModel = new ReportEmailModel
                    {
                        recepients = emails.Select(e => e.Email).ToArray(),
                        EmailTitle = reportType.Name,
                        routineName = reportType.Name,
                        siteId = model.SiteId
                    };


                    var siteService = new SiteService();
                    var siteName = siteService.Get(model.SiteId).Name;

                    var dateTime = DateTime.Now.ToString("yyyyMMdd_HHmm");

                    var file = File.OpenRead(path);
                    this.Send(reportEmailModel, file, $"{report.ReportType.Name.Replace("/", "").Replace(" ","_")}_{siteName}_{dateTime}.pdf");
                    file.Close();

                    //delete the file
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }

                }
                catch (Exception ex)
                {
                  
                }
            };
        }

        private void Send(ReportEmailModel model, Stream stream, string fileName)
        {
            var emailService = new EmailService();
            var siteService = new SiteService();

            model.routineName = Regex.Replace(model.routineName, @"\s+", "").Replace("/", "");
            if(model.routineName == "IB-LarmochRyckrapport(internal)")
            {
                model.routineName = "IB-LarmochRyckrapport";
            }
            var template = emailService.GetEmailTemplateFromRoutineName(model.routineName);
            var name = "";
            if (model.siteId != 0)
            {
                var site = siteService.Get(model.siteId);
                name = site.Name;
            }

            if (model.recepients != null)
            {              
                template.Subject = template.Subject.Replace("{siteName}", name).Replace("{date}",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Service.Tools.Email.SendEmailWithAttachmentToMultipleEmails("", model.recepients.ToList(), template.Subject, template.Body, string.Empty, stream, fileName,null,false,null, $"SER4 - {name}");                   
            }
            
        }
    }
}
