﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentTypeController : ApiController
    {
        private Type1Service _service;
        public IncidentTypeController()
        {
            _service = new Type1Service();
        }

        [HttpGet]
        [Route("IncidentType/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetType1(id);
                return new ResponseDataModel<Type1Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident Type",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IncidentType/GetAll")]
        public IHttpActionResult GetAll(int id, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllType1(id, siteId).OrderBy(x=>x.Name);
                return new ResponseDataModel<IEnumerable<Type1Model>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IncidentType/GetReportTypes")]
        public IHttpActionResult GetReportTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetReportTypes().OrderBy(x=>x.Name);
                return new ResponseDataModel<IEnumerable<ReportTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Report Types",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("IncidentType/GetReportTypesForScheduler")]
        public IHttpActionResult GetReportTypesForScheduler()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetReportTypes().OrderBy(x => x.Name);
                return new ResponseDataModel<IEnumerable<ReportTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Report Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IncidentType/Save")]
        public IHttpActionResult Save([FromBody]Type1Model model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _service.AddType1(model);
                return new ResponseDataModel<Type1Model>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Incident Type":"Type 1 already exists",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IncidentType/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var success = _service.DeleteType1(id);
                var message = success ? "Successfully Deleted Type1" : "Cannot delete Type1 because it is associated to a daily report.";
                return new ResponseDataModel<Type1Model>
                {
                    Success = success,
                    StatusCode = HttpStatusCode.OK,
                    Message = message,
                };
            }));
        }
    }
}
