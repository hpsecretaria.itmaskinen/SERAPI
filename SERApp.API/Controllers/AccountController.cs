﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class AccountController : ApiController
    {
        private AccountService _accountService;
        private AccountSettingsService _accountSettingService;
        public AccountController() {
            _accountService = new AccountService();
            _accountSettingService = new AccountSettingsService();
        }

        [HttpGet]
        [Route("Account/Get")]
        [ResponseType(typeof(ResponseDataModel<AccountModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.Get(id);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/GetAdmin")]
        [ResponseType(typeof(ResponseDataModel<UserModel>))]
        public IHttpActionResult GetAdmin(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetAdmin(id);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Admin User for this Account",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<AccountModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetAll();
                return new ResponseDataModel<List<AccountModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Accounts",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Account/Save")]
        public IHttpActionResult SaveAccount(AccountModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                if (string.IsNullOrEmpty(model.ContactNumber))
                {
                    model.ContactNumber = model.Mobile;
                }
                
                var data = _accountService.SaveAccount(model);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Account Record",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Account/Delete")]
        public IHttpActionResult DeleteAccount(AccountModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountService.DeleteAccount(model.Id);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Account Record"
                };
            }));
        }

        [HttpGet]
        [Route("Account/IsDuplicate")]
        public IHttpActionResult Get(string companyName)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.IsDuplicate(companyName);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Extras = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/Settings")]
        [ResponseType(typeof(ResponseDataModel<List<AccountSettingModel>>))]
        public IHttpActionResult GetAccountSettings(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountSettingService.GetAccountSettings(accountId);
                return new ResponseDataModel<List<AccountSettingModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Account Settings",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Account/SaveSettings")]
        public IHttpActionResult SaveSettings(AccountSettingModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountSettingService.Save(model);
                return new ResponseDataModel<AccountSettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Save Account Settings"
                };
            }));
        }
    }
}
