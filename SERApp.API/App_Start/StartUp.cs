﻿using Hangfire;
using Microsoft.Owin;
using Owin;
using SERApp.Models.Constants;
using System.Configuration;

[assembly: OwinStartup(typeof(SERApp.API.StartUp))]

namespace SERApp.API
{
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
            GlobalConfiguration.Configuration.UseSqlServerStorage(connectionString);

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}