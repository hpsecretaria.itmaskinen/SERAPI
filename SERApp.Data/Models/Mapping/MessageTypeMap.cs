﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class MessageTypeMap : EntityTypeConfiguration<MessageType>
    {
        public MessageTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ConfirmationName)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MessageTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ConfirmationName).HasColumnName("ConfirmationName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IsUsed).HasColumnName("IsUsed");
        }
    }
}
