﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IssueType1Map : EntityTypeConfiguration<IssueType1>
    {
        public IssueType1Map()
        {
            // Primary Key
            this.HasKey(t => t.IssueType1Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("IssueType1");
            this.Property(t => t.IssueType1Id).HasColumnName("IssueType1Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
