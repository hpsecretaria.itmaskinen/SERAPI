﻿using System.Data.Entity.ModelConfiguration;
namespace SERApp.Data.Models.Mapping
{
    public class EmailTemplateMap : EntityTypeConfiguration<EmailTemplate>
    {

        public EmailTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("EmailTemplates");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoutineName).HasColumnName("RoutineName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.SMSBody).HasColumnName("SMSBody");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.IsSystem).HasColumnName("IsSystem");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
        }
    }
}
