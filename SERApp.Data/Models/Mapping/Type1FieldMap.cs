﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class Type1FieldMap : EntityTypeConfiguration<Type1Field>
    {
        public Type1FieldMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Label)
                .HasMaxLength(500);

            this.ToTable("Type1_Fields");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type1Id).HasColumnName("Type1Id");
            this.Property(t => t.FieldId).HasColumnName("FieldId");
            this.Property(t => t.Label).HasColumnName("Label");
            this.Property(t => t.DataSource).HasColumnName("DataSource");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.IsRequired).HasColumnName("IsRequired");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.ExcludedValues).HasColumnName("ExcludedValues");
            this.Property(t => t.CustomScript).HasColumnName("CustomScript");

            this.HasRequired(t => t.DailyReportField)
                .WithMany(t => t.Type1Fields)
                .HasForeignKey(t => t.FieldId);
        }
    }
}
