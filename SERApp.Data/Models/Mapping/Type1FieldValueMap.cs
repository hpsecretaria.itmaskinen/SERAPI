﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class Type1FieldValueMap : EntityTypeConfiguration<Type1FieldValue>
    {
        public Type1FieldValueMap()
        {
            this.HasKey(t => t.Id);

            this.ToTable("Field_Values");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IncidentId).HasColumnName("IncidentId");
            this.Property(t => t.Type1_FieldId).HasColumnName("Type1_FieldId");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Type).HasColumnName("Type");
        }
    }
}
