﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class ConfigSettingMap : EntityTypeConfiguration<ConfigSetting>
    {
        public ConfigSettingMap() {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("ConfigSettings");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SettingName).HasColumnName("SettingName");
            this.Property(t => t.SettingLabel).HasColumnName("SettingLabel");
            this.Property(t => t.DefaultValue).HasColumnName("DefaultValue");
            this.Property(t => t.InputType).HasColumnName("InputType");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");

            
        }
    }
}
