﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class AccountEmailTemplateMap : EntityTypeConfiguration<AccountEmailTemplate>
    {
        public AccountEmailTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("AccountEmailTemplates");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EmailTemplateId).HasColumnName("EmailTemplateId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.SMSBody).HasColumnName("SMSBody");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.LanguageId).HasColumnName("LanguageId");
            
        }
    }
}
