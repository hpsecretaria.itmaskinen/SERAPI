using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class MessageAuditMap : EntityTypeConfiguration<MessageAudit>
    {
        public MessageAuditMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RoutineName)
                .HasMaxLength(150);

            this.Property(t => t.ContentSubject)
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            this.Property(t => t.Status)
                .HasMaxLength(10);

            this.Property(t => t.SentFrom)
                .HasMaxLength(100);

            this.Property(t => t.SentTo)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MessageAudit");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SourceId).HasColumnName("SourceId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.RoutineName).HasColumnName("RoutineName");
            this.Property(t => t.ContentSubject).HasColumnName("ContentSubject");
            this.Property(t => t.ContentBody).HasColumnName("ContentBody");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.SentFrom).HasColumnName("SentFrom");
            this.Property(t => t.SentTo).HasColumnName("SentTo");
            this.Property(t => t.SentDate).HasColumnName("SentDate");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
        }
    }
}
