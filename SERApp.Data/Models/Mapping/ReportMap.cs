﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class ReportMap : EntityTypeConfiguration<Report>
    {
        public ReportMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Reports");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ReportTypeId).HasColumnName("ReportTypeId");
            this.Property(t => t.TenantId).HasColumnName("TenantId");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.Cause).HasColumnName("Cause");
            this.Property(t => t.GuardId).HasColumnName("GuardId");
            this.Property(t => t.AdditionalText).HasColumnName("AdditionalText");

            this.Property(t => t.PoliceReport).HasColumnName("PoliceReport");
            this.Property(t => t.Resolution).HasColumnName("Resolution");
            this.Property(t => t.PoliceAuthority).HasColumnName("PoliceAuthority");
            this.Property(t => t.Plaintiff).HasColumnName("Plaintiff");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.IncidentText).HasColumnName("IncidentText");
            this.Property(t => t.IncidentId).HasColumnName("IncidentId");
            this.Property(t => t.By).HasColumnName("By");
            this.Property(t => t.OrderNumber).HasColumnName("OrderNumber");

            this.Property(t => t.AlarmTime).HasColumnName("AlarmTime");
            this.Property(t => t.GuardTime).HasColumnName("GuardTime");
            this.Property(t => t.ActionTime).HasColumnName("ActionTime");

            this.Property(t => t.VisibleEffect).HasColumnName("VisibleEffect");
            this.Property(t => t.AlarmReset).HasColumnName("AlarmReset");
            this.Property(t => t.PoliceCalled).HasColumnName("PoliceCalled");
            this.Property(t => t.KManInformed).HasColumnName("KManInformed");
            this.Property(t => t.KManUnavailable).HasColumnName("KManUnavailable");

            this.HasRequired(t => t.Incident)
                .WithMany(t => t.Reports)
                .HasForeignKey(t => t.IncidentId);
        }
    }
}
