﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class RyckrapportDataSectionMap : EntityTypeConfiguration<RyckrapportDataSection>
    {
        public RyckrapportDataSectionMap() {
            // Primary Key
            this.HasKey(t => t.RyckrapportDataSectionId);


            // Table & Column Mappings
            this.ToTable("RyckrapportDataSection");
            this.Property(t => t.RyckrapportDataSectionId).HasColumnName("RyckrapportDataSectionId");
            this.Property(t => t.SectionId).HasColumnName("SectionId");
            this.Property(t => t.RyckrapportDataId).HasColumnName("RyckrapportDataId");

            //this.HasRequired(x => x.RyckrapportData).WithMany(x => x.RyckrapportDataSections).HasForeignKey(x => x.RyckrapportDataId);
            this.HasRequired(x => x.Section).WithMany(x => x.RyckrapportDataSections).HasForeignKey(x => x.SectionId);
        }
    }
}
