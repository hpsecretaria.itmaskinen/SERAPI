﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class TaskMainCategoryMap : EntityTypeConfiguration<TaskMainCategory>
    {
        public TaskMainCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaskMainCategory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.MainCategoryId).HasColumnName("MainCategoryId");
            this.Property(t => t.TaskId).HasColumnName("TaskId");
   

            // Relationships
            this.HasRequired(t => t.Task)
                .WithMany(t => t.TaskMainCategory)
                .HasForeignKey(d => d.TaskId);

            this.HasRequired(t => t.MainCategory)
                .WithMany(t => t.TaskMainCategories)
                .HasForeignKey(d => d.MainCategoryId);

        }
    }
}
