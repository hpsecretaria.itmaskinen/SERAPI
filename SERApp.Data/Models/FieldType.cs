﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public partial class FieldType
    {
        public FieldType()
        {
            this.DailyReportFields = new List<DailyReportFields>();
        }

        public int PkFieldTypeId { get; set; }
        public string Flag { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<DailyReportFields> DailyReportFields { get; set; }
    }
}
