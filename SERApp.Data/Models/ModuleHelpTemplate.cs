﻿namespace SERApp.Data.Models
{
    public class ModuleHelpTemplate
    {
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public string Name { get; set; }
        public string ContentValue { get; set; }
    }
}
