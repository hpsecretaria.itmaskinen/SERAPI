﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Incident
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int Quantity { get; set; }
        public int PoliceResponse { get; set; }

        public string ActionDescription { get; set; }

        public int? Type1Id { get; set; }
        public virtual Type1 Type1 { get; set; }

        public int? Type2Id { get; set; }
        public virtual Type2 Type2 { get; set; }

        public int? Type3Id { get; set; }
        public virtual Type3 Type3 { get; set; }

        public int? TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }

        public virtual ICollection<IncidentGuard> IncidentGuard { get; set; }
        public virtual ICollection<Report> Reports { get; set; }

        public int AccountId { get; set; }
        public int SiteId { get; set; }

        public bool IsDeleted { get; set; }

        public bool WithBlueLight { get; set; }
        public string BlueLightOption { get; set; }

    }
}
