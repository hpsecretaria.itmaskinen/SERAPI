﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IssueType3
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public bool Value { get; set; }
        public int AccountId { get; set; }
    }
}
