﻿using SERApp.Service.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Helpers
{
    public static class JsonResponseHelper
    {
        public static string JsonSuccess(string message, object extra = null)
        {
            var returnObject = new ResponseModel
            {
                Success = true,
                Message = message,
                Extras = extra
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnObject);
        }

        public static string JsonError(string message, int errorCode)
        {
            var returnObject = new ResponseModel
            {
                Success = false,
                ErrorCode = errorCode,
                Message = message
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnObject);
        }
    }
}
