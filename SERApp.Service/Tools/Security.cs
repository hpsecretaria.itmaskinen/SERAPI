﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Tools
{
    public class Security
    {
        private static Random random = new Random();
        private const int SALT_SIZE = 8;
        private const int NUM_ITERATIONS = 1000;
        private static readonly RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        /// <summary>
        /// GENERATE 8 CHAR Random Password
        /// </summary>
        /// <returns></returns>
        public static string GenerateTempPassword() {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateRandomAlphanumberChars() {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars.ToLower(), 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Creates a hashed password
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string CreateSaltedHashedPassword(string password)
        {
            byte[] buf = new byte[SALT_SIZE];
            rng.GetBytes(buf);
            string salt = Convert.ToBase64String(buf);

            Rfc2898DeriveBytes deriver2898 = new Rfc2898DeriveBytes(password.Trim(), buf, NUM_ITERATIONS);
            string hash = Convert.ToBase64String(deriver2898.GetBytes(16));
            return salt + ':' + hash;
        }
        /// <summary>
        /// checks if the password matches with the hashed password in the database
        /// </summary>
        /// <param name="password"></param>
        /// <param name="hashedPassword"></param>
        /// <returns></returns>
        public static bool IsPasswordValid(string password, string hashedPassword)
        {
            string[] parts = hashedPassword.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                return false;
            byte[] buf = Convert.FromBase64String(parts[0]);
            Rfc2898DeriveBytes deriver2898 = new Rfc2898DeriveBytes(password.Trim(), buf, NUM_ITERATIONS);
            string computedHash = Convert.ToBase64String(deriver2898.GetBytes(16));
            return parts[1].Equals(computedHash);
        }

        public static string ChangePasswordLink(string userName, string password) {
            return encrypt64baseUserPass(userName, password);
        }

        public static string encrypt64baseUserPass(string userName, string password) {
            var userNameBytes = System.Text.Encoding.UTF8.GetBytes(userName);
            var passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
            return Convert.ToBase64String(userNameBytes) + ":" + Convert.ToBase64String(passwordBytes);
        }

        public static string encrypt64baseLoanEmail(string email)
        {
            var emailBytes = System.Text.Encoding.UTF8.GetBytes(email);
            return Convert.ToBase64String(emailBytes);
        }

        public static string decrypt64BasedString(string encryptedString) {
            byte[] data = Convert.FromBase64String(encryptedString);
            string decodedString = Encoding.UTF8.GetString(data);
            return decodedString;
        }
    }
}
