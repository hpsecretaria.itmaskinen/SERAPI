﻿using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace SERApp.Service.Tools
{
    public class SMS
    {
        LogRepository _logRepository;
        public SMS()
        {
            _logRepository = new LogRepository();
        }
        public async Task<SMSResponseModel> SendCreateLoandConfirmation(LoanModel model)
        {
            return await SendLoanSMS(model, EmailTemplates.LoanCreate);
        }
        public async Task<SMSResponseModel> SendLoanUpdateNotification(LoanModel model)
        {
            return await SendLoanSMS(model, EmailTemplates.LoanUpdate);
        }

        public async Task<SMSResponseModel> SendLoanNotifcation(LoanModel model)
        {
            return await SendLoanSMS(model, EmailTemplates.LoanNotify);
        }

        public async Task<SMSResponseModel> SendLoanPassedDateNotification(LoanModel model)
        {
            return await SendLoanSMS(model, EmailTemplates.LoanPassedDate);
        }

        public async Task<SMSResponseModel> SendLoanConfirm(LoanModel model)
        {
            return await SendLoanSMS(model, EmailTemplates.LoanConfirm);
        }

        public async Task<SMSResponseModel> SendLoanReturn(LoanModel model)
        {
            return await SendLoanSMS(model, EmailTemplates.LoanReturn);
        }

        public async Task<SMSResponseModel> SendLoanSMS(LoanModel model, string loanRoutine)
        {
            string appUrl = ConfigurationManager.AppSettings["appurl"];
            string subject = string.Empty;
            string body = string.Empty;
            Email email = new Email();
            var emailTemplate = email.getAccountEamilTemplate(loanRoutine, model.Site.AccountId);

            body = StripHTML(emailTemplate.SMSBody);

            body = body.Replace("{accountName}", model.Site.Account.Name);
            body = body.Replace("{siteName}", model.Site.Name);

            //if (loanRoutine == EmailTemplates.LoanCreate)
            //{
            body = body.Replace("{link}", appUrl + "confirm/loan?code=" + Security.encrypt64baseLoanEmail(model.Customer.Email) + ":" + model.Id);
            //}

            body = body.Replace("{customerName}", model.Customer.Name);

            body = body.Replace("{loanId}", model.LoanId);
            body = body.Replace("{loanItemNameId}", model.ItemNameId);
            body = body.Replace("{loanDescription}", model.Description);
            body = body.Replace("{loanGivenBy}", model.GivenBy);
            body = body.Replace("{loanReceivedBy}", model.ReceivedBy);
            body = body.Replace("{loanedDate}", model.LoanedDate.ToShortDateString());
            body = body.Replace("{confirmedDate}", model.ConfirmedDate.HasValue && model.ConfirmedDate != DateTime.MinValue ? model.ConfirmedDate.Value.ToShortDateString() : "N / A");
            body = body.Replace("{loanConfirmedDate}", model.ConfirmedDate.HasValue && model.ConfirmedDate != DateTime.MinValue ? model.ConfirmedDate.Value.ToShortDateString() : "N / A");
            body = body.Replace("{returnedDate}", model.ReturnedDate.HasValue && model.ReturnedDate != DateTime.MinValue ? model.ReturnedDate.Value.ToShortDateString() : "N / A");
            body = body.Replace("{actualReturnedDate}", model.ActualReturnedDate.HasValue && model.ActualReturnedDate != DateTime.MinValue ? model.ActualReturnedDate.Value.ToShortDateString() : "N / A");

            return await SendSMS(model.Site.Account.Company, model.Customer.MobileNumber, body);
        }

        public async Task<SMSResponseModel> SendSMS(string from, string mobileNumber, string message)
        {
            from = "SER4_SYSTEM";
            var smsModel = new OutgoingSMSModel()
            {
                From = from,
                Numbers = new List<string>() { mobileNumber },
                Message = message,
                Prio = 1,
                Email = false
            };
            string smsParams = Newtonsoft.Json.JsonConvert.SerializeObject(smsModel);
            return await SendSMS(smsParams);
        }

        public async Task<SMSResponseModel> SendSMS(string smsParams)
        {
            using (var client = new HttpClient())
            {
                string smsAccount = ConfigurationManager.AppSettings["ip1Account"];
                string smsKey = ConfigurationManager.AppSettings["ip1Key"];

                client.BaseAddress = new Uri("http://api.ip1sms.com/api/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", smsAccount, smsKey))));

                byte[] messageBytes = System.Text.Encoding.UTF8.GetBytes(smsParams);
                var content = new ByteArrayContent(messageBytes);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync("sms/send", content).Result;
                string responseContent = await response.Content.ReadAsStringAsync();
                string responseJSON = responseContent.TrimStart('[').TrimEnd(']');

                return Newtonsoft.Json.JsonConvert.DeserializeObject<SMSResponseModel>(responseJSON);
            }
        }

        public static string StripHTML(string body)
        {
            return Regex.Replace(body, "<.*?>", string.Empty);
        }
    }
}
