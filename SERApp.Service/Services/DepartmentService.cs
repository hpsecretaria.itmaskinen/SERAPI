﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class DepartmentService
    {
        DepartmentRepository _departmentRepository;
        LogRepository _logRepository;
        public DepartmentService()
        {
            _departmentRepository = new DepartmentRepository();
            _logRepository = new LogRepository();
        }

        public DepartmentModel Get(int id) {
            try
            {
                var data = _departmentRepository.Get(id);
                return entityToModel(data);
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
                
        }

        public List<DepartmentModel> GetAllByAccountId(int accountId) {
            try
            {
                var data = _departmentRepository.GetAllDepartmentsByAccountId(accountId);
                return data.Select(d => new DepartmentModel()
                {
                    Id = d.Id,
                    Name = d.Name,
                    DepartmentHead = d.DepartmentHead,
                    ContactNumber = d.ContactNumber,
                    Description = d.Description,
                    AccountId = d.AccountId,
                    CreatedDate = d.CreatedDate
                })
                .OrderBy(d => d.Name)
                .ToList();
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, accountId, 0);
                throw ex;
            }
            
        }

        public bool SaveDepartment(DepartmentModel model) {
            try
            {

                if (_departmentRepository.GetAllDepartmentsByAccountId(model.AccountId).Any(r => r.Name == model.Name))
                {
                    return false;
                }
                _departmentRepository.SaveDepartment(model);
            } catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, model.AccountId, 0);
                throw ex;
            }

            return true;
        }

        public void DeleteDepartment(int id) {
            try
            {
                _departmentRepository.Delete(id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
                throw ex;
            }
        }
        
        public DepartmentModel entityToModel(Department entity) {
            return new DepartmentModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                DepartmentHead = entity.DepartmentHead,
                ContactNumber = entity.ContactNumber,
                Description = entity.Description,
                AccountId = entity.AccountId,
                CreatedDate = entity.CreatedDate
            };
        }
    }
}
