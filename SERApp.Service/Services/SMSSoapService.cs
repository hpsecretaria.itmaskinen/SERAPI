﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class SMSSoapService
    {
        private smscomWS.sendSmsSoapClient SMSHandler;

        public SMSSoapService()
        {
            SMSHandler = new smscomWS.sendSmsSoapClient();
        }

        private string LastReturnedValue { get; set; }

        public void Send(Int32 UseAccountForSiteID, string TargetNumber, string TextToSend)
        {

            string SenderNumber = "";
            string SMSAccount = "";
            string SMSAPIKey = "";

            //using (var TA = new SMSDSTableAdapters.tblSiteTableAdapter())
            //{
            //    SMSDS.tblSiteRow dt = TA.GetDataBySiteID(UseAccountForSiteID)[0];
            //    SenderNumber = dt.SMSSender;
            //    SMSAccount = dt.SMSAccount;
            //    SMSAPIKey = dt.SMSAPIKey;
            //}


            string CleanTargetNumber = MobileNumFix(TargetNumber);
            string CleanSenderNumber = MobileNumFix(SenderNumber);

            if (IsMobileNumber(CleanTargetNumber))
            {
                LastReturnedValue = SMSHandler.sms(SMSAccount, SMSAPIKey, CleanTargetNumber, CleanSenderNumber, TextToSend, "1");
            }
        }

        public void Send(string SenderNumber, string TargetNumber, string TextToSend, string SMSAccount, string SMSAPIKey)
        {

            string CleanTargetNumber = MobileNumFix(TargetNumber);
            string CleanSenderNumber = MobileNumFix(SenderNumber);

            if (IsMobileNumber(CleanTargetNumber))
            {
                LastReturnedValue = SMSHandler.sms(SMSAccount, SMSAPIKey, CleanTargetNumber, CleanSenderNumber, TextToSend, "1");
            }
        }

        public void SendBulk(Int32 UseAccountForSiteID, string TargetNumbers, string TextToSend)
        {

            string SenderNumber = "";
            string SMSAccount = "";
            string SMSAPIKey = "";

            //using (var TA = new SMSDSTableAdapters.tblSiteTableAdapter())
            //{
            //    SMSDS.tblSiteRow dt = TA.GetDataBySiteID(UseAccountForSiteID)[0];
            //    SenderNumber = dt.SMSSender;
            //    SMSAccount = dt.SMSAccount;
            //    SMSAPIKey = dt.SMSAPIKey;
            //}

            char[] delimiterChars = { ',' };
            string[] nrs = TargetNumbers.Split(delimiterChars);
            string[] FixedNumbers = (from string n in nrs where IsMobileNumber(MobileNumFix(n)) select MobileNumFix(n)).ToArray(); // Join(From, ((string)(n)), In, nrs, Select, MobileNumFix(n)).ToArray;                        
            string CleanSenderNumber = MobileNumFix(SenderNumber);
            string CleanTargetNumbers = string.Join(",", FixedNumbers);
            SMSHandler.smsBundle(SMSAccount, SMSAPIKey, CleanTargetNumbers, CleanSenderNumber, TextToSend, "1");
        }

        public void SendBulk(String SenderNumber, string TargetNumbers, string TextToSend, string SMSAccount, string SMSAPIKey)
        {

            char[] delimiterChars = { ',' };
            string[] nrs = TargetNumbers.Split(delimiterChars);
            string[] FixedNumbers = (from string n in nrs where IsMobileNumber(MobileNumFix(n)) select MobileNumFix(n)).ToArray(); // Join(From, ((string)(n)), In, nrs, Select, MobileNumFix(n)).ToArray;                        
            string CleanSenderNumber = MobileNumFix(SenderNumber);
            string CleanTargetNumbers = string.Join(",", FixedNumbers);
            SMSHandler.smsBundle(SMSAccount, SMSAPIKey, CleanTargetNumbers, CleanSenderNumber, TextToSend, "1");
        }

        private bool IsNumeric(string text)
        {
            double test;
            return double.TryParse(text, out test);
        }

        public string MobileNumFix(string _MobileNum)
        {
            string MobileNum = _MobileNum;
            if (MobileNum.StartsWith("0"))
            {
                MobileNum = MobileNum.Remove(0, 1);
                MobileNum = MobileNum.Insert(0, "46");
            }

            MobileNum = MobileNum.Replace("+46", "");
            MobileNum = MobileNum.Replace("(0)", "");
            MobileNum = MobileNum.Replace(" ", "");
            MobileNum = MobileNum.Replace("-", "");

            if (IsNumeric(MobileNum))
            {
                if (!MobileNum.StartsWith("46"))
                {
                    MobileNum = MobileNum.Insert(0, "46");
                    MobileNum = MobileNum.Trim();
                }
            }

            return MobileNum;
        }

        //public string GetSaldo(Int32 UseAccountForSiteID)
        //{
        //    smscomWS.sendSmsSoapClient SMSHandler = new smscomWS.sendSmsSoapClient();
        //    using (var TA = new SMSDSTableAdapters.tblSiteTableAdapter())
        //    {
        //        SMSDS.tblSiteRow dt = TA.GetDataBySiteID(UseAccountForSiteID)[0];

        //        string SMSAccount = dt.SMSAccount;
        //        string SMSAPIKey = dt.SMSAPIKey;
        //        return SMSHandler.balans(SMSAccount, SMSAPIKey);
        //    }

        //}

        public string GetSaldo(string SMSAccount, string SMSAPIKey)
        {
            smscomWS.sendSmsSoapClient SMSHandler = new smscomWS.sendSmsSoapClient();
            return SMSHandler.balans(SMSAccount, SMSAPIKey);

        }

        public bool IsMobileNumber(string nr)
        {
            // Return True
            nr = MobileNumFix(nr);
            if (((Int64.Parse(nr) > 0) && nr.StartsWith("467")))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
