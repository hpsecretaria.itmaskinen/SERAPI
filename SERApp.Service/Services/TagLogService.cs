﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ITagLogService
    {
        IEnumerable<TagLogModel> GetTagLogs(int accountId = 0, int siteId = 0, string tagLogDate = "");
        bool AddTagLog(TagLogModel model);
        void DeleteTagLogById(int id);
        TagLogModel GetTagLogById(int id);
    }
    public class TagLogService : ITagLogService
    {
        private ITagLogRepository _repository;
        private ITagRepository _tagRepository;
        private IGuardRepository _guardRepository;
        public TagLogService()
        {
            _repository = new TagLogRepository();
            _guardRepository = new GuardRepository();
            _tagRepository = new TagRepository();
        }


        public bool AddTagLog(TagLogModel model)
        {
            try
            {

                var tag = _tagRepository.GetByPredicate(x => x.RFIDData.ToLower().Equals(model.Tag.RFIDData.ToLower())).ToList();
                var guard = _guardRepository.Get(model.GuardId);
                if (tag.Any())
                {
                    if (guard != null)
                    {
                        var data = tag.FirstOrDefault();
                        _repository.Save(new TagLog()
                        {
                            GuardId = model.GuardId,
                            TagId = data.Id,
                            Time = DateTime.Now
                        });
                    }
                    else {
                        return false;
                    }                 
                }
                else {
                    return false;
                }
                
             
                return true;
            } catch (Exception ex)
            {
                return false;
            }
            
        }

        public void DeleteTagLogById(int id)
        {
            _repository.Delete(id);
        }

        public TagLogModel GetTagLogById(int id)
        {
            
            var x = _repository.GetTagLogsIncludeChilds(id);

           return new TagLogModel()
           {
               GuardId = x.GuardId,
               Id = x.Id,
               TagId = x.TagId,
               Time = x.Time,
               Tag = new TagModel()
               {
                   Id = x.Tag.Id,
                   Description = x.Tag.Description,
                   RFIDData = x.Tag.RFIDData,
                   Tenant = new TenantModel()
                   {
                       Id = x.Tag.Tenant.Id,
                       Name = x.Tag.Tenant.Name,   
                       Site = new SiteModel()
                       {
                           Id = x.Tag.Tenant.Site.Id,
                           Name = x.Tag.Tenant.Site.Name
                       }
                   },
               },
               Guard = new GuardModel()
               {
                   Id = x.Guard.Id,
                   Name = x.Guard.Name,
               }
           };
        }

        public IEnumerable<TagLogModel> GetTagLogs(int accountId = 0, int siteId = 0, string tagLogDate = "")
        {
            var data = _repository.GetAllTagLogsIncludeChilds();

            if (accountId != 0)
            {                
                data = data.Where(x => x.Tag.Tenant.Site.AccountId.Equals(accountId));
            }

            if (siteId != 0)
            {
                data = data.Where(x => x.Tag.Tenant.SiteId.Equals(siteId));
            }

            if (!string.IsNullOrEmpty(tagLogDate))
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParse(tagLogDate, out date))
                {            
                    data = data.Where(x => x.Time.ToShortDateString().Equals(date.ToShortDateString()));
                }
                    
            }

            return data.Select(x => new TagLogModel()
            {
                GuardId = x.GuardId,
                Id = x.Id,
                TagId = x.TagId,
                Time = x.Time,
                Tag = new TagModel()
                {
                    Id = x.Tag.Id,
                    Description = x.Tag.Description,
                    RFIDData = x.Tag.RFIDData,
                    Tenant = new TenantModel()
                    {
                        Id = x.Tag.Tenant.Id,
                        Name = x.Tag.Tenant.Name,                        
                    },                    
                },
                Guard = new GuardModel()
                {
                    Id = x.Guard.Id,
                    Name = x.Guard.Name,                    
                }
            }).ToList();
        }
    }
}
