﻿(function (ko) {
    var componentId = "loan-list";

    var getLoans = function () {
        return $.ajax({
            url: '/Loan/GetAll',
            type: 'json',
            method: 'get'
        });
    }

    var ViewModel = function (params) {
        self.loans = ko.observableArray([]);
        self.firstCustomer = [];
        self.gridClass = ko.observable('col-lg-8 col-md-8 col-sm-8 pad-x-5');
        self.statusData = ko.observableArray(statuses);

        self.facilities = ko.observableArray([]);
        self.types = ko.observableArray([]);

        
        self.initGrid = function () {
            //Exportable table
            var table = $('#list-loan-grid').DataTable({
                responsive: true,
                //autoWitdh: false,
                destroy: true,
                columns: [
                    {
                        title: '', className: 'table-actions', data: 'Status', render: function (data) {
                            var template = '';
                            if (data === 'NotConfirmed') {
                                template = '<span><i class="fa fa-exclamation-circle primary"></i></span>';
                            }
                            else if (data === 'PassedReturnedData') {
                                template = '<span><i class="fa fa-exclamation-triangle warning"></i></span>';
                            }
                            else if (data === 'Returned') {
                                template = '<span><i class="fa fa-check-circle success"></i></span>';
                            }
                            else if (data === 'Deleted') {
                                template = '<span><i class="fa fa-times-circle danger"></i></span>';
                            }
                            return template;
                        }
                    },
                    {
                        title: 'Created', data: 'CreatedDate', render: function (data) {
                            return moment(data).format('DD-MM-YYYY');
                        }
                    },
                    { title: 'Facility', data: 'Customer.Facility.FacilityName' },
                    { title: 'Type', data: 'LoanType.Name' },
                    { title: 'Customer', data: 'Customer.Name' },
                    {
                        title: 'Start', data: 'LoanedDate', render: function (data) {
                            return moment(data).format('DD-MM-YYYY');
                        }
                    },
                    {
                        title: 'Returned', data: 'ReturnedDate', render: function (data) {
                            return data ? moment(data).format('DD-MM-YYYY') : '';
                        }
                    },
                    {
                        title: 'Actions', data: 'Id', className: 'table-actions', render: function (data, type, rowData) {
                            var template = '<a href="javascript:editLoan(' + data + ');"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>';        
                            template += '<a href= "javascript:void(0);" > <span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>';
                            return template;
                        }
                    }
                ],
                data: self.loans(),
                "order": [],
                "createdRow": function (row, data, dataIndex) {
                    if (data.ReturnedDate) {
                        $(row).addClass('returned-loans');
                    }
                    else {
                        $(row).addClass('unreturned-loans');
                    }
                },
                "dom": '<"top"f>rt<"bottom"ilp><"clear">',
                "iDisplayLength": 10,
                "pagingType": "simple_numbers",
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [0] /* 1st one, start by the right */
                }],
                "language": {
                    "search": "",
                    "searchPlaceholder": "Quick Search"
                }
            });
            $('#list-loan-grid thead').addClass('table-header');
            var template = $('#list-loan-grid_filter').detach().appendTo('#search-section');

            $('#list-loan-grid tbody').on('click', 'tr', function () {
                $('.loading').show();
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }

                var selectedRowData = table.row('.selected').data();
                var selectedRowFromList = self.loans().filter(function (item) {
                    return item.Id === selectedRowData.Id
                })
                if (selectedRowFromList[0]) self.customerDetails(selectedRowFromList[0]);
                $('.loading').hide();
            });
        }

        self.bindData = function () {
            getFacilities().done(function (response) {
                var data = JSON.parse(response).data;
                self.facilities(data);
            });

            getLoanTypes().done(function (response) {
                var data = JSON.parse(response).data;
                self.types(data);
            })
        }

        self.getLoanData = function () {
            getLoans().done(function (response) {
                var responseData = JSON.parse(response);
                var data = responseData.data;
                self.loans(data);
                self.initGrid();
                self.customerDetails(data[0]);
            })
        };

        self.customerDetails = function (data) {
            var loan = data;
            self.gridClass('col-lg-8 col-md-8 col-sm-8 pad-x-5');
            var param = {
                Loan: loan,
                Source: 'list-preview'
            }
            _.defer(function () {
                ko.ext.renderComponent("#loan-details-container", "loan-details", param);
            })
            
        }

        self.editLoan = function (id) {
            var param = {
                loanId: id,
                Source: 'list-edit'
            }
            _.defer(function () {
                ko.ext.renderComponent("#main-component-container", "loan-edit", param);
            })
        }

        self.init = function () {
            self.bindData();
            self.getLoanData();
            
        }

        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);