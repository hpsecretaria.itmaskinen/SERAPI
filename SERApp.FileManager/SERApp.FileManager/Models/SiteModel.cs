﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SER.FileManager.Models
{
    public class SiteModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}