﻿using Microsoft.VisualStudio.DebuggerVisualizers;
using SER.FileManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SER.FileManager.Repository
{
    public class SiteRepository
    {
        private string connString;

        public SiteRepository()
        {
            connString = ConfigurationManager.ConnectionStrings["SERAppDBContext"].ConnectionString;
        }
        
        public List<SiteModel> SitesPerAccount(int accountId)
        {
            var result = new List<SiteModel>();

            using (var connection = new SqlConnection(connString))
            {
                var query = $@"SELECT Id, Name FROM Sites WHERE AccountId = {accountId}";

                var command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteModel
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    Name = reader[1].ToString(),
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public SiteFolderRole FindSiteFolderRole(int siteId, string folder, int userId)
        {
            var result = default(SiteFolderRole);

            using (var connection = new SqlConnection(connString))
            {
                var query = @" 
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.Folder = @folder AND SFR.UserId = @userId";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);
                    command.Parameters.AddWithValue("@folder", folder);
                    command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                };

                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId, string folder)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.Folder = @folder";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);
                    command.Parameters.AddWithValue("@folder", folder);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }
        
        public List<SiteFolderRole> FindSiteFolderRole(int siteId, int userId)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.UserId = @userId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);
                command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> SiteFolderRole(int siteId, string path)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = $@"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE (Folder = '{path}' OR Folder LIKE '{path}\%') AND SFR.SiteId = @siteId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId 
WHERE SFR.SiteId = @siteId";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public void CreateSiteFolderRole(SiteFolderRole folderRole)
        {
            var existingRole = this.FindSiteFolderRole(folderRole.SiteId, folderRole.Folder, folderRole.UserId);
            var query = string.Empty;

            if (folderRole.RoleId != -1 && existingRole != null)
            {
                query = $"UPDATE [SiteFolderRights] SET RoleId = ${folderRole.RoleId} WHERE [Id]={existingRole.Id}";
            }
            else if (folderRole.RoleId != -1)
            {
                query = $"INSERT INTO [SiteFolderRights] (SiteId, Folder, UserId, RoleId) VALUES ({folderRole.SiteId}, '{folderRole.Folder}', {folderRole.UserId}, {folderRole.RoleId})";
            }
            else if (folderRole.RoleId == -1 && existingRole != null)
            {
                query = $"DELETE FROM [SiteFolderRights] WHERE [Id]={existingRole.Id}";
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateSiteFolderRole(SiteFolderRole folderRole)
        {
            var query = $"UPDATE [SiteFolderRights] SET [Folder] = '{folderRole.Folder}' WHERE [Id]={folderRole.Id}";

            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery();
                }
            }
        }

        public void DeleteSiteFolderRole(int siteId, string path)
        {
            var query = $@"DELETE FROM [SiteFolderRights] WHERE (Folder = '{path}' OR Folder LIKE '{path}\%')  AND SiteId = {siteId}";

            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery();
                }
            }
        }
    }
}
