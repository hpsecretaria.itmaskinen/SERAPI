﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class AccountController : ApiController
    {
        private AccountService _accountService;
        public AccountController() {
            _accountService = new AccountService();
        }

        [HttpGet]
        [Route("Account/Get")]
        [ResponseType(typeof(ResponseDataModel<AccountModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.Get(id);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<AccountModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetAll();
                return new ResponseDataModel<List<AccountModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Accounts",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Account/Save")]
        public IHttpActionResult SaveAccount(AccountModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.SaveAccount(model);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Account Record",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Account/Delete")]
        public IHttpActionResult DeleteAccount(AccountModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountService.DeleteAccount(model.Id);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Account Record"
                };
            }));
        }
    }
}
