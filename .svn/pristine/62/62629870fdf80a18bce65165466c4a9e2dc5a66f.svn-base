﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class SettingController : ApiController
    {
        private readonly ISettingService _service;
        public SettingController()
        {
            _service = new SettingService();
        }

        [HttpGet]
        [Route("Setting/GetAll")]
        public IHttpActionResult GetAll(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSettings(accountId);
                return new ResponseDataModel<IEnumerable<SettingModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Setting/GetValue")]
        public IHttpActionResult GetValue(string setting, int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetValue(setting, accountId);
                return new ResponseDataModel<SettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Setting/GetValueById")]
        public IHttpActionResult GetValueById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetValueById(id);
                return new ResponseDataModel<SettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Setting/Save")]
        public IHttpActionResult Save([FromBody]SettingModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.Save(model);
                return new ResponseDataModel<SettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = model
                };
            }));
        }
    }
}
