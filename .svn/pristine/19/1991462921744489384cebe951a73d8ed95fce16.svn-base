﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentController : ApiController
    {
        private IncidentService _service;
        public IncidentController()
        {
            _service = new IncidentService();
        }

        [HttpGet]
        [Route("Incident/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidents();
                return new ResponseDataModel<IEnumerable<IncidentModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Incident/Save")]
        public IHttpActionResult Save(IncidentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddIncident(model);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Incident",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Incident/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident",
                };
            }));
        }
    }
}
