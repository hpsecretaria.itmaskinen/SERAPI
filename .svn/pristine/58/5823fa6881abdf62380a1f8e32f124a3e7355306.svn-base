﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IGuardService
    {
        IEnumerable<GuardModel> GetGuards(int SiteId);
        void DeleteGuardById(int id);
        void AddGuard(GuardModel model);
        GuardModel GetGuardById(int id);
    }
    public class GuardService : IGuardService
    {
        private IRepository<Guard> _repository;
        public GuardService()
        {
            _repository = new Repository<Guard>();
        }


        public void AddGuard(GuardModel model)
        {                        
            _repository.Save(new Guard()
            {
                CreatedDate = DateTime.Now,
                Name = model.Name,
                SiteId = model.SiteId
            });
        }

        public void DeleteGuardById(int id)
        {
            _repository.Delete(id);
        }

        public GuardModel GetGuardById(int id)
        {
           var value = _repository.Get(id);

           return new GuardModel()
           {
               CreatedDate = value.CreatedDate,
               Id = value.Id,
               IncidentGuard = value.IncidentGuard.Select(x=>new IncidentGuardModel() { }).ToList(),
               LastUpdatedDate = value.LastUpdatedDate,
               Name = value.Name,              
               SiteId = value.SiteId
           };
        }

        public IEnumerable<GuardModel> GetGuards(int SiteId)
        {
           return _repository.GetByPredicate(x => x.SiteId == SiteId)
                .Select(x=>new GuardModel()
                {
                    CreatedDate = x.CreatedDate,
                    Id= x.Id,
                    LastUpdatedDate = x.LastUpdatedDate,
                    Name= x.Name,
                    SiteId = x.SiteId
                })
                .ToList();
        }
    }
}
