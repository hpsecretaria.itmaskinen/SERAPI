﻿using GleamTech.FileUltimate;
using SER.FileManager.Models;
using SER.FileManager.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace SER.FileManager.Presenter
{
    public class FileManagerPresenter
    {
        SiteRepository _siteRepository;
        UserRepository _userRepository;
        UserRoleModuleRepository _userRoleModuleRepository;
        GleamTech.FileUltimate.FileManager fileManager;
        public FileManagerPresenter()
        {
            fileManager = new GleamTech.FileUltimate.FileManager();
            _siteRepository = new SiteRepository();
            _userRepository = new UserRepository();
            _userRoleModuleRepository = new UserRoleModuleRepository();
        }

        public void EnsurePath(string path)
        {
            if (System.IO.Directory.Exists(path) == false)
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        public IEnumerable<string> GetAvailableDirectories(string rootFolder) {
            var directories = Directory.GetDirectories(rootFolder);
            return directories;
        }

        public List<UserModuleAndRoleModel> GetModuleAndRoles(int userId) {
            return _userRoleModuleRepository.GetModuleAndRoles(userId);
        }

        public List<UserModuleAndRoleModel> GetModuleAndRolesForAdmin(int userId)
        {
            return _userRoleModuleRepository.GetModuleAndRolesForAdmin(userId);
        }

        public List<ModuleModel> GetModules() {
            return _userRoleModuleRepository.GetModules();
        }

        public Dictionary<int, string> GetRoles()
        {
            return _userRoleModuleRepository.GetRoles();
        }

        public UserModel GetUser(int userId) {
            return _userRepository.GetUser(userId);
        }

        public List<UserModel> GetUsersForModule(int moduleId)
        {
            return _userRepository.GetUsersForModule(moduleId);
        }

        public SiteFolderRole SiteFolderRole(int siteId, string folder, int userId)
        {
            return this._siteRepository.FindSiteFolderRole(siteId, folder, userId);
        }

        public List<SiteFolderRole> SiteFolderRole(int siteId, int userId)
        {
            return this._siteRepository.FindSiteFolderRole(siteId, userId);
        }

        public List<SiteFolderRole> SiteFolderRole(int siteId, string path)
        {
            return this._siteRepository.SiteFolderRole(siteId, path);
        }

        public void CreateSiteFolderRole(SiteFolderRole folderRole)
        {
            this._siteRepository.CreateSiteFolderRole(folderRole);
        }

        public void UpdateSiteFolderRole(SiteFolderRole folderRole)
        {
            this._siteRepository.UpdateSiteFolderRole(folderRole);
        }

        public void DeleteSiteFolderRole(int siteId, string path)
        {
            this._siteRepository.DeleteSiteFolderRole(siteId, path);
        }

        public bool ValidateUser(string hashKey)
        {
            if (string.IsNullOrEmpty(hashKey))
            {
                return false;
            }
            return _userRepository.UserExists(hashKey);
        }

        public bool CanAccess(string userId)
        {
            return true;
        }
    }
}