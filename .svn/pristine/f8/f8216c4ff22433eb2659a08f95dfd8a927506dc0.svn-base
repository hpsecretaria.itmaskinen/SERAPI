import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { LoansConfirmComponent } from './components';

const routes: Routes = [
    { path: '', loadChildren: './components/layout/layout.module#LayoutModule', canActivate: [AuthGuard] },
    { path: 'login', loadChildren: './components/login/login.module#LoginModule' },
    { path: 'signup', loadChildren: './components/signup/signup.module#SignupModule' },
    { path: 'error', loadChildren: './components/server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './components/access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './components/not-found/not-found.module#NotFoundModule' },
    { path: 'confirm/loan', component: LoansConfirmComponent },
    { path: '**', redirectTo: 'not-found' },
];

@NgModule({
    imports: [FormsModule, RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
