using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SERApp.Data.Models.Mapping;

namespace SERApp.Data.Models
{
    public partial class SERAppDBContext : DbContext
    {
        static SERAppDBContext()
        {
            Database.SetInitializer<SERAppDBContext>(null);
        }

        public SERAppDBContext()
            : base("Name=SERAppDBContext")
        {
        }
        public SERAppDBContext(string connString)
           : base(!string.IsNullOrEmpty(connString) ? connString : "Name=SERAppDBContext")
        {
        }
        public DbSet<AccountModule> AccountModules { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<MessageAudit> ConfirmationAudits { get; set; }
        public DbSet<MessageType> MessageTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<LoanType> LoanTypes { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<UserModuleRoles> UserModuleRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Tenant> Tenants { get; set; }

        //
        public DbSet<CustomType> CustomTypes { get; set; }
        public DbSet<Guard> Guards { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<IncidentGuard> IncidentGuard { get; set; }
        public DbSet<IncidentType> IncidentTypes { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportType> ReportTypes { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagLog> TagLogs { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountModuleMap());
            modelBuilder.Configurations.Add(new AccountMap());
            modelBuilder.Configurations.Add(new MessageAuditMap());
            modelBuilder.Configurations.Add(new MessageTypeMap());
            modelBuilder.Configurations.Add(new CustomerMap());
            modelBuilder.Configurations.Add(new FacilityMap());
            modelBuilder.Configurations.Add(new ItemMap());
            modelBuilder.Configurations.Add(new LoanMap());
            modelBuilder.Configurations.Add(new LoanTypeMap());
            modelBuilder.Configurations.Add(new ModuleMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new SiteMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new UserModuleRoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new LogMap());
            modelBuilder.Configurations.Add(new TenantMap());

            modelBuilder.Configurations.Add(new CustomTypeMap());
            modelBuilder.Configurations.Add(new GuardMap());
            modelBuilder.Configurations.Add(new IncidentMap());
            modelBuilder.Configurations.Add(new IncidentGuardMap());
            modelBuilder.Configurations.Add(new IncidentTypeMap());
            modelBuilder.Configurations.Add(new ReportMap());
            modelBuilder.Configurations.Add(new ReportTypeMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new TagLogMap());
     
        }
    }
}
