﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class TenantController : ApiController
    {
        private TenantService _tenantService;
        public TenantController()
        {
            _tenantService = new TenantService();
        }

        [HttpGet]
        [Route("Tenant/Get")]
        [ResponseType(typeof(ResponseDataModel<TenantModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.Get(id);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAll();
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllByAccountId")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllByAccountId(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllByAccountId(accountId);
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllBySiteId")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllBySiteId(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllBySiteId(siteId);
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllTenantContactsByTenantId")]
        [ResponseType(typeof(ResponseDataModel<List<ContactModel>>))]
        public IHttpActionResult GetAllTenantContactsByTenantId(int tenantId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllTenantContactsByTenantId(tenantId);
                return new ResponseDataModel<List<ContactModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Contacts for this Tenant",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveTenantContact")]
        public IHttpActionResult SaveTenantContact(TenantContactModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveTenantContact(model);
                return new ResponseDataModel<TenantContactModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Contact for this Tenant",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/Save")]
        public IHttpActionResult SaveUser(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveTenant(model);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tenant Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/Delete")]
        public IHttpActionResult DeleteAccount(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteTenant(model.Id);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Record"
                };
            }));
        }
    }
}
