using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public partial class MessageAuditModel
    {
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int UserId { get; set; }
        public string RoutineName { get; set; }
        public string ContentSubject { get; set; }
        public string ContentBody { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
