﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class TagLogModel
    {
        public int Id { get; set; }

        public int TagId { get; set; }
        public TagModel Tag { get; set; }

        public DateTime Time { get; set; }

        public int GuardId { get; set; }
        public GuardModel Guard { get; set; }


    }
}
