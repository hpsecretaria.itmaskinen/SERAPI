﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class TimeModel
    {
        public int hour { get; set; }
        public int minute { get; set; }
        public int second { get; set; }
    }
}
