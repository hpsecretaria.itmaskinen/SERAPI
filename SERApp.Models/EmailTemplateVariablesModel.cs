﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class EmailTemplateVariablesModel
    {
        public int Id { get; set; }
        public string VariableName { get; set; }
        public string VariableData { get; set; }
        public int EmailTemplateId { get; set; }
    }
}
