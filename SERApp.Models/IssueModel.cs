﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IssueModel
    {
        public int IssueId { get; set; }

        public string Object { get; set; }
        public string Tenant { get; set; }
        public string Yta { get; set; }
        public string Phone { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int ApartmentNumber { get; set; }
        public string Typ { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsPolice { get; set; }

        public int AccountId { get; set; }
        public bool IsSuccess { get; set; }
    }
}
