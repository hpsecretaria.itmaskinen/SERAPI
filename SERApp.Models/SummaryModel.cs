﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SummaryModel
    {
        public int SummaryId { get; set; }
        public DateTime Date { get; set; }
        public string SummaryValue { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
    }
}
