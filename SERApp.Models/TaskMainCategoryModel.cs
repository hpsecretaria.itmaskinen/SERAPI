﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class TaskMainCategoryModel
    {
        public int Id { get; set; }
        public int MainCategoryId { get; set; }
        public int TaskId { get; set; }

        public virtual MainCategoryModel MainCategory { get; set; }
        public virtual Task Task { get; set; }

    }
}
