﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class RyckrapportDataModel
    {
        public int RyckrapportDataId { get; set; }
        public int ReportId { get; set; }
        public int TenantId { get; set; }
       // public int SectionId { get; set; }
        public TimeModel Time { get; set; }

        public ReportModel Report { get; set; }
        public IEnumerable<RyckrapportDataSectionModel> RyckrapportDataSections { get; set; }
        public TenantModel Tenant { get; set; }
    }
}
